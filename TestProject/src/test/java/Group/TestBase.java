package Group;
import java.io.FileInputStream;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

public class TestBase {
	public static final SoftAssert softassert = new SoftAssert();
	public static WebDriver driver;
	public  WebElement element;
	public static Properties prop;
	public static String incrementor;
	@BeforeMethod
	public void read() throws Exception

	{

		prop = new Properties();
		try
		{
			FileInputStream fis = new FileInputStream("C:\\Users\\MUM1229\\eclipse-workspace\\TestProject\\src\\test\\java\\Group\\ConfigProperties");
			prop.load(fis);

		} 
		catch (Exception e)
		{

			e.printStackTrace();
		}



		if(prop.getProperty("Browser").equalsIgnoreCase("FireFox"))
		{	
			System.out.println("Gone into FireFox Loop");
			driver = new FirefoxDriver();
			driver.get(prop.getProperty("Url"));
			Thread.sleep(4000);
		}

		else if(prop.getProperty("Browser").equalsIgnoreCase("Chrome"))
		{
			System.out.println("Gone into Chrome Loop");
			System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(prop.getProperty("Url"));
			Thread.sleep(4000);
		}

		else if(prop.getProperty("Browser").equalsIgnoreCase("ie"))
		{
			System.out.println("Gone into Internet Explorer Loop");
			System.setProperty("webdriver.ie.driver", "D:\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			Thread.sleep(4000);
		}


		else
		{
			System.out.println("Gone into Else Loop");
		}

	}	

	public void wait(int seconds , String incrementor) 
	{
		final String incrementor1 = prop.getProperty("incrementor");
		System.out.println("Value of Incrementor is  : "+incrementor1);
		try
		{
			synchronized (driver)
			{
				System.out.println(" Waiting for "+seconds+" seconds + " +incrementor1+" seconds ");
				int number=Integer.parseInt(incrementor1);
				seconds =(seconds*1000) + (Integer.parseInt(incrementor1))*1000;
				driver.wait(seconds);
			}

		}
		catch(InterruptedException e)
		{

			e.printStackTrace();

		}

	}		
	public void WaitForPageToLoad()
	{
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>()
		{
			public Boolean apply(WebDriver driver)
			{

				return((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
			}
		};

		try 
		{
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(driver , 30);
			wait.until(expectation);
		}
		catch(Throwable error)
		{

			Assert.fail(" Timeout waiting for Page Load Request to complete ");
		}

	}


	public static void waitForClickable(WebElement element , int maxWait , int  incrementor )
	{

		for(int i =0 ; i<=incrementor;i++)
		{
			try
			{
				//Fluent wait implementation here Polling time is also loaded from the properties
				Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(maxWait, TimeUnit.SECONDS).pollingEvery(maxWait, TimeUnit.SECONDS).ignoring(NoSuchElementException.class,
						StaleElementReferenceException.class );
				wait.until(ExpectedConditions.elementToBeClickable(element));
				break;

			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());

			}			
		}		
	}


	public static void waitForVisibility(WebElement element , int maxWait , int incrementor)
	{
		for(int i =0 ; i<=incrementor;i++)
		{
			try
			{
				//Fluent wait implementation 
				Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(maxWait, TimeUnit.SECONDS).pollingEvery(maxWait, TimeUnit.SECONDS).ignoring(NoSuchElementException.class,
						StaleElementReferenceException.class);
				wait.until(ExpectedConditions.elementToBeClickable(element));
				break;

			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());

			}		

		}

	}

	@AfterMethod
	public void After()
	{
		driver.quit();
	}





}
