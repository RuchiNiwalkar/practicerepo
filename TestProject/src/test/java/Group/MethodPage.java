package Group;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.taskdefs.WaitFor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
public class MethodPage extends TestBase{

	PageTest page = new PageTest(driver);
	FileInputStream fis ;

	XSSFWorkbook wb ;

	XSSFSheet sheet1 ;

	public void Login() throws InterruptedException 

	{

		Thread.sleep(3000);
		page.UserNameBDO.sendKeys("aspadmin@bdo.in");
		wait(2, incrementor);

		page.PasswordBDO.sendKeys("1234");

		waitForClickable(page.LoginBDO, 2, 2);
		page.LoginBDO.click();
		Thread.sleep(6000);

	}

	public void SelectGroup() throws InterruptedException 

	{
		Thread.sleep(3000);
		page.sidebaricon.click();
		Thread.sleep(3000);
		page.group.click();
		Thread.sleep(3000);
		softassert.assertEquals(page.Verifygroupandacc.getText().trim() , "Existing Accounts/Groups" , "Group and accounts page ");		
	
	}

	public void SelectTRP() throws InterruptedException 

	{

		Thread.sleep(3000);
		page.sidebaricon.click();
		Thread.sleep(3000);
		page.Trp.click();
		Thread.sleep(3000);
		softassert.assertEquals(page.VerifyTrp.getText().trim() , "Existing TRPs" , "TRPs  page ");		
	}

	public void AddGroup() 
	{
		wait(2, incrementor);
		page.Addgroup.click();
		wait(2, incrementor);
		softassert.assertEquals(page.VerifyAddAccorgroup.getText().trim(), "Add Account/Group (*Mandatory field)" , "Add Account/Group not validated");

	}

	public void BasicInfo() throws Exception
	{


		File src = new File("C:\\ExcelData\\TestDataExcel.xlsx");

		fis = new FileInputStream(src); 

		wb = new XSSFWorkbook(fis);

		sheet1 = wb.getSheetAt(0);

		int num  = sheet1.getLastRowNum();
		System.out.println("THE LAST ROW COUNT IS : "+num);

		for( int i=0 ; i<=num ; i++)

		{

			String value  = sheet1.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				String accno  = sheet1.getRow(i).getCell(1).toString();
				page.AddAccName.sendKeys(accno);
				Thread.sleep(3000);

			}

			else if(value.equalsIgnoreCase("Email-ID"))
			{
				String email  = sheet1.getRow(i).getCell(1).toString();
				page.Email.sendKeys(email);
				Thread.sleep(3000);
			}


			else if(value.equalsIgnoreCase("MobileNumber"))
			{
				String MobileNum  = sheet1.getRow(i).getCell(1).toString();
				page.Mobile.sendKeys(MobileNum);
				Thread.sleep(3000);
			}



			else if(value.equalsIgnoreCase("Address Type"))
			{
				page.AddressType.click();
				Thread.sleep(2000);
				page.ListofAddrstyp.get(0).click();
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Premises"))
			{
				page.Premises.click();
				Thread.sleep(2000);
				page.ListofPremises.get(0).click();
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Address Line 1"))
			{
				String AddressLine1  = sheet1.getRow(i).getCell(1).toString();
				System.out.println("The Address Line 1 is : "+AddressLine1);
				page.AddressLine1.sendKeys(AddressLine1);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Address Line 2"))
			{
				String AddressLine2  = sheet1.getRow(i).getCell(1).toString();
				System.out.println("The Address Line 2 is : "+AddressLine2);
				page.AddressLine2.sendKeys(AddressLine2);
				Thread.sleep(3000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(3000);
				page.State_Dropdown.click();
				Thread.sleep(2000);

				for(int a=0 ; a<page.ListofStates.size() ; a++)
				{
					if(page.ListofStates.get(a).getText().equalsIgnoreCase(sheet1.getRow(i).getCell(1).toString()))
					{

						page.ListofStates.get(a).click();
						Thread.sleep(6000);
						break;
					}

				}
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("CityDistrict_Type"))
			{
				System.out.println("Entered City Loop");
				page.City_District.click();
				Thread.sleep(3000);

				for(int b=0 ; b<page.City_DistrictList.size() ; b++)
				{
					if(page.City_DistrictList.get(b).getText().equalsIgnoreCase("Mumbai"))
					{

						page.City_DistrictList.get(b).click();
						Thread.sleep(6000);
						break;
					}

				}
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("PIN Code"))
			{
				String PINCode  = sheet1.getRow(i).getCell(1).toString();
				page.PINCode.sendKeys(PINCode);
				Thread.sleep(2000);
			}


			else
			{
				System.out.println("Fail to take data from excel");
			}

		}

		Thread.sleep(6000);


	}


	public void adminUser() throws Exception
	{
		Thread.sleep(3000);
		page.NextButton.click();
		Thread.sleep(3000);

		File src = new File("C:\\ExcelData\\TestDataExcel.xlsx");

		fis = new FileInputStream(src); 

		wb = new XSSFWorkbook(fis);

		XSSFSheet sheet2 = wb.getSheetAt(1);

		int NumRows  = sheet2.getLastRowNum();

		System.out.println("The total Num of Rows in Admin Users : "+NumRows);

		for( int i=0 ; i<=NumRows ; i++)

		{

			String value  = sheet2.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Email-ID"))
			{
				String Email  = sheet2.getRow(i).getCell(1).toString();
				page.Email_AdminUser.sendKeys(Email);
				Thread.sleep(3000);


			}


			else if(value.equalsIgnoreCase("MobileNumber"))
			{
				String MobNum  = sheet2.getRow(i).getCell(1).toString();
				page.Mobile_AdminUser.sendKeys(MobNum);
				Thread.sleep(3000);

			}


			else if(value.equalsIgnoreCase("PAN"))
			{
				String Pan  = sheet2.getRow(i).getCell(1).toString();
				page.Pan_AdminUser.sendKeys(Pan);
				Thread.sleep(3000);

			}


			else if(value.equalsIgnoreCase("First_Name"))
			{
				String Firstname  = sheet2.getRow(i).getCell(1).toString();
				page.Firstname_AdminUser.sendKeys(Firstname);
				Thread.sleep(3000);

			}

			else if(value.equalsIgnoreCase("Last_Name"))
			{
				String Lastname  = sheet2.getRow(i).getCell(1).toString();
				page.Lastname_AdminUser.sendKeys(Lastname);
				Thread.sleep(3000);

			}


			else if(value.equalsIgnoreCase("Aadhaar"))
			{
				String Aadhar  = sheet2.getRow(i).getCell(1).toString();
				page.Aadhaar_AdminUser.sendKeys(Aadhar);
				Thread.sleep(3000);

			}



			else
			{

				System.out.println("Failed for Admin User");
			}


		}

		wb.close();
		page.Save_AdminUser.click();
		Thread.sleep(22000);
		page.OK.click();
		Thread.sleep(10000);

	}

	public void ValidateRecord(String number) throws Exception
	{
	
		page.Recordsperpagedropdown.click();
		Thread.sleep(4000);
		int num = page.Recordsperpage.size();
		System.out.println("The size of records per page is : "+num);

		for(int i=0 ; i<num ; i++)
		{
			if(page.Recordsperpage.get(i).getText().equalsIgnoreCase(number))
			{
				page.Recordsperpage.get(i).click();
				Thread.sleep(3000);
				break;
			}

		}
		
	}

	public void ScrollPage(String Value)
	{	

		JavascriptExecutor jse = (JavascriptExecutor)driver;


		if(Value=="Verticalscrolldown")
		{

			//Vertical scroll DOWN 
			jse.executeScript("window.scrollBy(0,400)", "");
		}

		if(Value=="Verticalscrollup")
		{

			//Vertical scroll UP 
			jse.executeScript("window.scrollBy(0,-450)", "");
		}

		if(Value=="Horizontalscrollright")
		{

			//HORIZONTAL scroll RIGHT 
			jse.executeScript("window.scrollBy(20,0)", "");
		}


		if(Value=="Horizontalscrolllefts")
		{

			//HORIZONTAL scroll LEFT 
			jse.executeScript("window.scrollBy(-40,0)", "");
		}

	}



	public void ViewGroup() throws Exception
	{


		ScrollPage("Verticalscrolldown");

		int view = page.ViewGroup.size();

		System.out.println("The ViewList is :- "+view);

		int viewList = view-1;

		System.out.println("The ViewList Element required is :- "+viewList);

		Thread.sleep(6000);

		page.ViewGroup.get(viewList).click();

		Thread.sleep(3000);



		VerifyViewGroup();


	}

	public void VerifyViewGroup() throws Exception
	{



		softassert.assertEquals(page.MobileView.getText().trim(), "9619388801", "The mobile number is not verified");

		softassert.assertEquals(page.StateView.getText().trim(), "Maharashtra", "The State name is not verified");

		

		Thread.sleep(3000);

	}




	public void Mailinator() throws Exception
	{
		driver.get("http://www.mailinator.com");	

		Thread.sleep(3000);

		File src = new File("C:\\ExcelData\\TestDataExcel.xlsx");

		fis = new FileInputStream(src); 

		wb = new XSSFWorkbook(fis);

		sheet1 = wb.getSheetAt(1);

		int num  = sheet1.getLastRowNum();

		System.out.println("THE LAST ROW COUNT IS : "+num);

		String Mail_id = null;
		for(int i =0; i <num ; i++)
		{
			String Column2value = sheet1.getRow(i).getCell(1).toString();
			System.out.println(Column2value);
			if(Column2value.endsWith(".com"))
			{
				Mail_id = sheet1.getRow(i).getCell(1).toString();
				break;
			}

		}

		System.out.println("The Mail id required is :- "+Mail_id);

		page.MailLogin.sendKeys(Mail_id);

		Thread.sleep(2000);


		page.Go.click();


		Thread.sleep(7000);


		softassert.assertEquals(page.Verifyemail.getText().trim(), Mail_id , "Email not verfied for mailinator");

		

	}



	public void ActivationLink() throws Exception
	{
		Thread.sleep(10000);
		System.out.println("THE FIRST INDEX IS :- "+page.MailinatorList.get(0).getText());
		page.MailinatorList.get(0).click();
		Thread.sleep(15000);
		page.MailLink.click();	
		Thread.sleep(15000);
		driver.switchTo().frame(0) ;

		Thread.sleep(15000);

		String ActivationUrl = page.CopyUrlLink.getText();
		String ActualLink = ActivationUrl.substring(5 , (ActivationUrl.length())-1 );

		System.out.println("The Required link is  : "+ActualLink);

		driver.switchTo().defaultContent();
		Thread.sleep(15000);

		//Store the current window handle

		String winHandleBefore = driver.getCurrentUrl();

		System.out.println("The winHandleBefore is  : "+winHandleBefore);

		driver.get(ActualLink);

		driver.manage().window().maximize();

		Thread.sleep(7000);

		page.CloseButton.click();

		Thread.sleep(7000);

		page.SendOTP.click();

		Thread.sleep(10000);

		softassert.assertEquals(page.OTPSucess.getText().trim(), "OTP sent sucessfully", "OPT not sent successfully");

		Thread.sleep(2000);

		driver.get(winHandleBefore);

		Thread.sleep(10000);

		System.out.println("WINHANDLE BEFORE : "+winHandleBefore);

		Thread.sleep(15000);

		page.MailinatorList.get(0).click();

		Thread.sleep(15000);

		page.ListOfOTP.get(0).click();

		Thread.sleep(8000);

		driver.switchTo().frame(0);

		Thread.sleep(2000);
		String OTPLine =page.SelectOTP.getText().substring(0,(page.SelectOTP.getText().length())-1);
		String 	[] parts =OTPLine.split(" ") ;

		String OTP = parts[5];
		System.out.println("THE OTP IS : "+OTP);

		driver.switchTo().defaultContent();
		Thread.sleep(5000);

		String Previous = driver.getCurrentUrl();

		driver.get(ActualLink);


		driver.manage().window().maximize();

		Thread.sleep(7000);

		page.CloseButton.click();

		Thread.sleep(7000);

		page.EnterOTP.sendKeys(OTP);

		Thread.sleep(2000);

		page.ConfirmOTP.click();

		Thread.sleep(4000);

		page.ConfirmOTP_OK.click();

		Thread.sleep(4000);

		page.Agree.click();

		Thread.sleep(4000);

		page.OTPPassword.sendKeys("Jan@2018");

		Thread.sleep(4000);

		page.ConfirmPassword.sendKeys(sheet1.getRow(6).getCell(1).toString());

		Thread.sleep(4000);

		page.ActivationSubmit.click();

		System.out.println("SUCCESS");

		Thread.sleep(6000);

		page.PasswrdSet.click();

		

	}

	public void SelectionText() throws Exception

	{
		int accId = page.ListOfAccIDs.size();
		System.out.println("the no of rows are : "+accId);

		for(int i=0 ; i< accId ; i++)

		{
			if(page.ListOfAccIDs.get(i).getText().equalsIgnoreCase("maneesh.bhartia@statkraft.com"))
			{

				System.out.println("The selected ID is : "+page.ListOfAccIDs.get(i).getText());
				page.ListOfViewIDs.get(i).click();
				Thread.sleep(4000);
			}

		}


	}

	public void LoginAdmin() throws Exception
	{


		driver.get("http://test.enablegst.info");

		File src = new File("C:\\ExcelData\\TestDataExcel.xlsx");

		fis = new FileInputStream(src); 

		wb = new XSSFWorkbook(fis);

		sheet1 = wb.getSheetAt(1);

		int num  = sheet1.getLastRowNum();

		System.out.println("THE LAST ROW COUNT IS : "+num);
		String Mail_id = null;
		for(int i =0; i <num ; i++)
		{
			String Column2value = sheet1.getRow(i).getCell(1).toString();
			System.out.println(Column2value);
			if(Column2value.endsWith(".com"))
			{

				Mail_id = sheet1.getRow(i).getCell(1).toString();
				break;
			}


		}

		System.out.println("The Mail id required is :- "+Mail_id);


		Thread.sleep(5000);
		page.UserNameBDO.sendKeys(Mail_id);
		Thread.sleep(4000);
		page.PasswordBDO.sendKeys(sheet1.getRow(6).getCell(1).toString());
		Thread.sleep(2000);
		page.LoginBDO.click();
		Thread.sleep(5000);

	}


	public void Company() throws Exception
	{
		page.sidebaricon.click();
		Thread.sleep(4000);
		page.Company.click();
		Thread.sleep(4000);
		String Company = page.VerifyCompany.getText().trim();
		softassert.assertEquals(Company, "Company" , "Company not verified");
		page.AddCompany.click();
		Thread.sleep(4000);
		String AddCompany = page.VerifyAddCompany.getText().trim();
		softassert.assertEquals(AddCompany, "Add New Company (*Mandatory field)" , "Add New Company not verified");

	}







}
