package Group;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class PageTest extends TestBase{

	public PageTest(WebDriver driver)
	{

		PageFactory.initElements(driver, this);
	}
	
	//XPaths for LOGIN BDO
   /* Locator for User name for ClientAdmin */  
	@FindBy(how = How.XPATH, using="//div[@class='row wrapper pos-rel']/div/div/div[@class='col-md-10']/form/div[2]/div[1]/div/input[@name='LoginId']")
	WebElement UserNameBDO;

	 /* Locator for Password for ClientAdmin */  
	@FindBy(how = How.XPATH, using=".//*[@id='Password']")
	WebElement PasswordBDO;

	
	/* Locator for LOGIN */
	@FindBy(how = How.XPATH, using="//div[@class='col-md-12 col-12 l-h-2 text-cent']/input[@class='k-button k-danger login_button float-right' and  @value='Log In']")
	WebElement LoginBDO;

	
	

	//Xpaths for GROUP

	/* Locator for sidebar icon on the dashboard page  */
	@FindBy(how = How.XPATH, using="//span[@id='menu-toggle' and @class='toggle-left toggleSidebar']/i")
	WebElement sidebaricon;

	/* Locator for group from the list on the dashboard page  */
	@FindBy(how = How.XPATH, using=".//*[@id='menu']/li/a[contains(text(),'Group')]")
	WebElement group;

	/* Locator for group and accounts page on Group page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Existing Accounts/Groups')]")
	WebElement Verifygroupandacc;
	

	/* Locator for group from the list on the dashboard page  */
	@FindBy(how = How.XPATH, using=".//*[@id='menu']/li/a[contains(text(),'TRP')]")
	WebElement Trp;

	/* Locator for group and accounts page on Group page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Existing TRPs')]")
	WebElement VerifyTrp;

	/* Locator for +GROUP on Group page  */
	@FindBy(how = How.XPATH, using="//button [@class='k-button k-success m-r-0-imp float-right addGroupBtn trigger']")
	WebElement Addgroup;

	/* Locator for Verify add account or group on Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='adduser' and contains(text(),'Add Account/Group ')]")
	WebElement VerifyAddAccorgroup;


	//Xpaths for adding group data from Excel


	/* Locator for Account Name on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_account_name']")
	WebElement AddAccName;


	/* Locator for Email-ID  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_account_email']")
	WebElement Email;


	/* Locator for Mobile  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_account_mobile']")
	WebElement Mobile;

	/* Locator for Select Dropdowm AddressType  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addAccTab1']/div[2]/div/div[1]/div[1]/div/div/span/span/span[2]")
	WebElement AddressType;


	/* Locator for Select Dropdowm AddressType  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_address_type-list']/div[3]/ul/li")
	List <WebElement>  ListofAddrstyp;


	/* Locator for Select Dropdowm Premises  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addAccTab1']/div[2]/div/div[1]/div[2]/div/div/span/span/span[2]")
	WebElement Premises;


	/* Locator for Select Dropdowm Premises  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_premises_listbox']/li")
	List <WebElement>  ListofPremises;


	/* Locator for AddressLine1  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_add_line_1']")
	WebElement  AddressLine1;



	/* Locator for  AddressLine2 AddressType  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_add_line_2']")
	WebElement  AddressLine2;


	/* Locator for Select State Dropwdown on + Group page  */
	@FindBy(how = How.XPATH, using="//div[@class='col-12 accSpec']/div[4]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement  State_Dropdown;


	/* Locator for Select List of States from the Dropdowm State  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_state_listbox']/li")
	List <WebElement>  ListofStates;


	/* Locator for Select City_District Dropwdown on + Group page  */
	@FindBy(how = How.XPATH, using="//div[@class='col-12 accSpec']/div[4]/div/following-sibling::div[1]/div/div/span/span/span[@class='k-select']")
	WebElement  City_District;


	/* Locator for Select List of City_District from the Dropdowm City_District  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_city_listbox']/li")
	List <WebElement> City_DistrictList;


	/* Locator for PINCode on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_pincode']")
	WebElement  PINCode;

	/* Locator for Next Button on + Group page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addAcc2' and contains(text(),'Next')]")
	WebElement  NextButton;

	//Xpaths for Admin User on +Group page


	/* Locator for  Admin User  Email-ID  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_client_admin1_email']")
	WebElement Email_AdminUser;


	/* Locator for  Admin User   Mobile  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_client_admin1_mobile']")
	WebElement Mobile_AdminUser;

	/* Locator for  Admin User  PAN  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_client_admin1_pan']")
	WebElement Pan_AdminUser;


	/* Locator for  Admin User  First Name  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_client_admin1_fname']")
	WebElement Firstname_AdminUser;



	/* Locator for  Admin User Last Name  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_client_admin1_lname']")
	WebElement Lastname_AdminUser;



	/* Locator for  Admin User  Aadhaar  on + Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='add_client_admin1_uidai']")
	WebElement Aadhaar_AdminUser;


	/* Locator for  Admin User  Save  on + Group page  */
	@FindBy(how = How.XPATH, using="//div[@class='k-button k-success float-right accsave' and contains(text(),'Save')]")
	WebElement Save_AdminUser;


	/* Locator for OK for new Added Group + Group page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and @type='button' and contains(text(),'OK')]")
	WebElement OK;

	/* Locator for Close button for already existing group on  page  */
	@FindBy(how = How.XPATH, using="//div/div/span[contains(text(),'Error')]/parent::div/div[@class='k-window-actions']/a/span[@class='k-icon k-i-close']")
	WebElement CloseError;


	/*Locator for Added success pop up message on Group page  */
	@FindBy(how = How.XPATH, using=".//div[@data-role='confirm' and contains(text(),'Group has been created successfully')]")
	WebElement PopUp_Verify_Success;

	/*Locator for Existing User pop up message on Group page  */
	@FindBy(how = How.XPATH, using="//div[@class='error_messages' and contains(text(),'User 1 Already Exist, Account/Group  not added')]")
	WebElement PopUp_Verify_Fail;




	//Xpaths for record per page dropdown

	/* Locator for  Records per page  dropdown on Existing Accounts/Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clienEntitiesTab']/div/div[4]/span[1]/span/span/span[2]")
	WebElement Recordsperpagedropdown;


	/* Locator for  Records per page on Existing Accounts/Group page  */
	@FindBy(how = How.XPATH, using="html/body/div[10]/div/div[2]/ul/li")
	List <WebElement> Recordsperpage;



	/* Locator for  View a group Existing Accounts/Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientEntitiesGrid']/tbody/tr/td[7]/a[1]/i")
	List <WebElement> ViewGroup;

	//xpaths for view details page of the group

	/* Locator for  View a Account name in  group Existing Accounts/Group page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Account Name ')]/parent::div/div/span[contains(text(),'Pepsi')]")
	WebElement AccNameView;


	/* Locator for  View a Email in  group Existing Accounts/Group page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewAccTab1']/div/div/div/p[contains(text(),'Email-ID')]/parent::div/div/span[contains(text(),'test3131@mailinator.com')]")
	WebElement EmailView;


	/* Locator for  Mobile View in  group Existing Accounts/Group page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Mobile')]/parent::div/div/span[contains(text(),'9619388801')]")
	WebElement MobileView;


	/* Locator for  State View in  group Existing Accounts/Group page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'State')]/parent::div/div/span[contains(text(),'Maharashtra')]")
	WebElement StateView;


	//Xpaths for mallinator

	/* Locator for Mailinator Login */  
	@FindBy(how = How.XPATH, using=".//*[@id='inboxfield']")
	WebElement MailLogin;


	/* Locator for Mailinator Login */
	@FindBy(how = How.XPATH, using=".//*[@id='many_login_email']")
	WebElement Login;


	/* Locator for Mailinator Password */
	@FindBy(how = How.XPATH, using=".//*[@id='many_login_password']")
	WebElement Password;



	/* Locator for Mailinator Login button GO */
	@FindBy(how = How.XPATH, using="//button[@class='btn btn-dark' and contains(text(),'Go!')]")
	WebElement Go;


	/* Locator for Mailinator Login and verifying emailid  */
	@FindBy(how = How.XPATH, using=".//*[@id='query_data']/div[contains(text(),'com')]")
	WebElement Verifyemail;



	/* Locator for Mailinator List after Logi n  */
	@FindBy(how = How.XPATH, using="//ul[@class='lb_all_sub-list']/li")
	List <WebElement>  MailinatorList;


	/* Locator for Mail Link */
	@FindBy(how = How.XPATH, using="//div[@class='all_message-min_text all_message-min_text-3' and contains(text(),'Account Activation')]")
	WebElement  MailLink;


	/* Locator for Mailinator Login and copyLink  */
	@FindBy(how = How.XPATH, using="html/body/p[4]")
	WebElement CopyUrlLink;


	/* Locator for Close button on OTP Activation Page */
	@FindBy(how = How.XPATH, using=".//*[@id='activationHelp']/a[contains(text(),'Close')]")
	WebElement CloseButton;


	/* Locator for Send OTP OTP Activation Page */
	@FindBy(how = How.XPATH, using="//input[@value='Send OTP']")
	WebElement SendOTP;


	/* Locator for  List of OTP's after Login  */
	@FindBy(how = How.XPATH, using="//ul[@id='inboxpane']/li/div/div[@class='all_message-min_text all_message-min_text-3' and contains(text(),'OTP')]")
	List <WebElement>  ListOfOTP;

	/* Locator for SELECT OTP Activation Page */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'OTP for account activation is')]")
	WebElement SelectOTP;

	/* Locator for Enter OTP Activation Page */
	@FindBy(how = How.XPATH, using=".//*[@id='otp' and @type='password']")
	WebElement EnterOTP;

	/* Locator for Confirm OTP Activation Page */
	@FindBy(how = How.XPATH, using="//input[@class='k-button k-primary btn-block confirmOTP']")
	WebElement ConfirmOTP;


	/* Locator for Confirm OTP OK Activation Page */
	@FindBy(how = How.XPATH, using="//button[@class='k-button' and contains(text(),'OK')]")
	WebElement ConfirmOTP_OK;


	/* Locator for  List of OTP's after Login  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientEntitiesGrid']/tbody/tr/td[3]")
	List <WebElement>  ListOfAccIDs;

	
	/* Locator for  List of OTP's after Login  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientEntitiesGrid']/tbody/tr/td[7]/a[1]/i")
	List <WebElement>  ListOfViewIDs;

	/* Locator for  List of OTP's after Login  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientEntitiesGrid']/tbody/tr/td[3]")
	List <WebElement>  ListOfmailids;

	/* Locator for Agree Activation Page */
	@FindBy(how = How.XPATH, using=".//span[@class='custom-control-indicator']")
	WebElement Agree;


	/* Locator for Password Activation Page */
	@FindBy(how = How.XPATH, using=".//*[@id='password']")
	WebElement OTPPassword;


	/* Locator for Confirm Password Activation Page */
	@FindBy(how = How.XPATH, using=".//*[@id='confPassword']")
	WebElement ConfirmPassword;


	/* Locator for Activation Submit on Activation Page */
	@FindBy(how = How.XPATH, using=".//*[@id='submit']")
	WebElement ActivationSubmit;

	/* Locator for OTPSucess on Activation Page */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'OTP sent sucessfully ')]")
	WebElement OTPSucess;

	/* Locator for Password Set Successfully on Activation Page */
	@FindBy(how = How.XPATH, using="//button[@class='k-button' and contains(text(),'OK')]")
	WebElement PasswrdSet;



	
	//Xpaths for user already exist for GROUP
	
	/* Locator for user already exist for GROUP */
	@FindBy(how = How.XPATH, using=".//div[@class='error_messages' and contains(text(),'User 1 Already Exist, Account/Group  not added')]")
	WebElement ErrorMsg;


	/* Locator for CLOSE BUTTON for user already exist for GROUP */
	@FindBy(how = How.XPATH, using="//span[@class='k-icon k-i-close']")
	WebElement Closebutton;

	/* Locator for CLOSE GROUP for user already exist for GROUP */
	@FindBy(how = How.XPATH, using=".//*[@id='addUserModal']/div/div/div[1]/button")
	WebElement CloseGroup;

	
	
	
	//Xpaths for COMPANY
	
	/* Locator for Company from the list on the dashboard page  */
	@FindBy(how = How.XPATH, using="//a[contains(text(),'Company')]")
	WebElement Company;

	/* Locator for Company Tab page on Company page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Company')]")
	WebElement VerifyCompany;

	/* Locator for + Add Company on Company page  */
	@FindBy(how = How.XPATH, using="//button[@trigger-id='addcomP1']")
	WebElement AddCompany;


	/* Locator for Verify Add New Company on Company page  */
	@FindBy(how = How.XPATH, using="//h5[contains(text(),'Add New Company')]")
	WebElement VerifyAddCompany;


	/* Locator for Close Add New Company Tab on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='compAddModal']/div/div/div/button[contains(text(),'Close')]")
	WebElement CloseAddCompany;

	/* Locator for Next Add New Company Tab on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP1']/a[contains(text(),'Next')]")
	WebElement NextAddCompany;

	
	
	//Xpaths for TRP
	
	
	
	





}
