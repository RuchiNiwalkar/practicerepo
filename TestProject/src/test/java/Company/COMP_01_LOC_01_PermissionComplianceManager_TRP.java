package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_PermissionComplianceManager_TRP  extends CompanyMethods
{
	@Test
	public void PermissionComplianceManager_TRP() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Verify Group & Email-ID
		CM.ViewGroup_Email_Id_Num();

		//Select Users
		CM.SelectUsers();

		//Selects the TradeName from the specified TRP user
		CM.TRP_TradeNamePermission("trpinwardrole1@mailinator.com");

		//Login into Test
		CM.Login_TRP("trpinwardrole1@mailinator.com");

		//Select Group
		CM.SelectTRP();


		//Verify Outward Tab
		CM.OutwardClient_Enabled();

		//Verify Inward Tab
		CM.InwardClient_Enable();
				
		//Verify GSTR3B
		CM.GSTR3B_Enabled();
		
	
		//Utilities
		CM.utilities_CM();
		
		
		softassert.assertAll();

	}

	
	
}
