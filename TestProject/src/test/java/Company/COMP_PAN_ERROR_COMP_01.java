package Company;

import org.testng.annotations.Test;

public class COMP_PAN_ERROR_COMP_01 extends CompanyMethods
{
	@Test
	public void COMP_PAN_ERROR_COMP() throws Exception
	{
		
		CompanyMethods CM = new CompanyMethods();
		
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Company
		CM.AddCompany_PanError();

		//Add Address Company
		CM.AddAddress_PanError();

		//Add GSTN_Username Company
		CM.addGSTN_PanError();

		//Verify the company in the company List
		CM.HSN_ITC_PanError();

		//Verify the company in the company List	
		CM.VerifyPanError_Company();

		softassert.assertAll();
	}
}
