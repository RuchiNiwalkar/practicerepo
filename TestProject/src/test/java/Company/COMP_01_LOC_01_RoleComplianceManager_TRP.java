package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_RoleComplianceManager_TRP extends CompanyMethods
{
	@Test
	public void RoleComplianceManager_TRP() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		CM.Login_Roles("trpcomrole1@mailinator.com");

		CM.SelectCompany();

		CM.VerifyforRoles();

		softassert.assertAll();
		
	}

}

