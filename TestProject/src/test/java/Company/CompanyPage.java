package Company;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Group.TestBase;

public class CompanyPage extends TestBase {

	public CompanyPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}


	//XPaths for  Company LOGIN BDO
	/* Locator for Username for company */
	@FindBy(how = How.XPATH, using="//div[@class='row wrapper pos-rel']/div/div/div[@class='col-md-10']/form/div[2]/div[1]/div/input[@name='LoginId']")
	WebElement UserNameBDO;


	/* Locator for Password for company */
	@FindBy(how = How.XPATH, using=".//*[@id='Password']")
	WebElement PasswordBDO;


	/* Locator for LOGIN */
	@FindBy(how = How.XPATH, using="//div[@class='col-md-12 col-12 l-h-2 text-cent']/input[@class='k-button k-danger login_button float-right' and  @value='Log In']")
	WebElement LoginBDO;

	/* Locator for New Close Functionality for company */
	@FindBy(how = How.XPATH, using="html/body/div[13]/div[1]/div/a")
	WebElement NewCloseFunctionality;


	//Xpaths for Company

	/* Locator for sidebar icon on the dashboard page  */
	@FindBy(how = How.XPATH, using="//span[@id='menu-toggle' and @class='toggle-left toggleSidebar']/i")
	WebElement sidebaricon;

	/* Locator for Company from the list on the dashboard page  */
	@FindBy(how = How.XPATH, using=".//*[@id='menu']/li/a[contains(text(),'Company')]")
	WebElement Company;

	/* Locator for Company page on Company page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Company')]")
	WebElement Verifycompany;

	/* Locator for Add Company on Company page  */
	@FindBy(how = How.XPATH, using="//button[@trigger-id='addcomP1']")
	WebElement Addcompany;


	//Xpaths for ADDING a New Company data from excel



	/* Locator for Account/Group on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_account_name' and @readonly='']")
	WebElement VerifyAcc_GroupNameComp;

	/* Locator for CompanyName on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_name']")
	WebElement AddCompanyName;


	/* Locator for TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_avg_turnover' and  @class='k-textbox']")
	WebElement TurnOver16_17;


	/* Locator for Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_email' and @class='k-textbox' and @type='email']")
	WebElement Registered_Email_id;

	/* Locator for TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_tradename' and @type='text']")
	WebElement TradeName;

	/* Locator for Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP1']/div[5]/div[1]/div/div/span/span/span[2]")
	WebElement  State_Dropdown;


	/* Locator for Select List of States from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_main_state_listbox']/li")
	List <WebElement>  ListofStates;


	/* Locator for GSTIN on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_gstin' and @class='k-textbox' and @type='text']")
	WebElement GSTIN;


	/* Locator for PAN on  Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_pan' and @class='k-textbox' and  @type='text']")
	WebElement PAN;

	/* Locator for TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_curr_turnover' and @name='addComp_curr_turnover']")
	WebElement TurnOver_AprilJune2017;

	/* Locator for Mobile  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_mobile' and @class='k-textbox' and @type='text']")
	WebElement Mobile;


	/* Locator for Select Constitution of Business  Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP1']/div[4]/div[2]/div/div/div/div/span/span/span[2]")
	WebElement  ConstOfBussiness;


	/* Locator for Select List of Select Constitution of Business  from Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_constitution_listbox']/li")
	List <WebElement>  ListConstOfBussiness;

	/* Locator for Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP1']/div[5]/div[2]/div/div/span/span/span[2]")
	WebElement  City_District_Dropdown;

	/* Locator for Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[5]/div[2]/div/div/span/span[@class='k-dropdown-wrap k-state-disabled']")
	WebElement  City_District_Dropdown_stateinactive;


	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_main_city-list']/div[@class='k-list-scroller']/ul/li")
	List <WebElement>  ListofCity_District;


	/* Locator for Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_main_city-list']/div[@class='k-list-scroller']/ul/li[contains(text(),'Aheri')]")
	WebElement  City_Aheri;


	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[7]/div/a[2]/i[@clear-id='edit_client_sel_gstin']")
	List <WebElement>  ListofUsers;

	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[4]")
	List <WebElement>  Listofmailsid;

	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr[7]/td")
	List <WebElement>  ListofrequiredRow;

	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[7]/div/a/i[@class='k-icon k-i-eye k-icon-20 col-1 view_client_admin_user tabsToggle' and @hide-id='deactivate']")
	List <WebElement>  ListofClientadmins;

	/* Locator for Next on Company page  */
	@FindBy(how = How.XPATH, using="//a[@id='next3' and contains(text(),'Next')]")
	WebElement  Next;

	//Xpaths for ADDRESS in ADD COMPANY

	/* Locator for Next on Company page  */
	@FindBy(how = How.XPATH, using="//a[@id='next4' and contains(text(),'Next')]")
	WebElement  NextRegistration ;


	/* Locator Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_add_line_1' and @class='clearEdit k-textbox']")
	WebElement  address1;

	/* Locator Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_add_line_2' and @class='clearEdit k-textbox']")
	WebElement  address2;

	/* Locator Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_pincode']")
	WebElement  pincode_address;


	/* Locator Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[1]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_address;


	/* Locator  Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_address_type_listbox']/li")
	List <WebElement>  ListddlAddress;

	/* Locator Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[1]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_Premises;


	/* Locator  Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_premises_listbox']/li")
	List <WebElement>  ListddlPremises;


	/* Locator States  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[4]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_States;


	/* Locator  States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_state_listbox']/li")
	List <WebElement>  ListddlStates;

	/* Locator City  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[4]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_City;


	/* Locator  City  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_city_listbox']/li")
	List <WebElement>  ListddlCity;

	/* Locator BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div[2]/span/span/span[@class='k-select']")
	WebElement  dropdown_BussinessActvty;


	/* Locator  BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_nature_listbox']/li")
	List <WebElement>  ListBussinessActvty;


	/* Locator Next_Adress on add address Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addcomP4' and contains(text(),'Next')]")
	WebElement  Next_Address;


	/* Locator GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_username']")
	WebElement  GSTN_username;


	/* Locator GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addcompP4']/div[2]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  GSTN_username1;

	/* Locator GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addcompP4']/div[3]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  GSTN_username2;


	/* Locator GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_auth1_email_listbox']/li")
	List <WebElement>  GSTN_users;


	/* Locator Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addComp_auth1_name']")
	WebElement  Name_GSTN;

	/* Locator Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addComp_auth1_mobile']")
	WebElement  Number_GSTN;


	/* Locator Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addComp_auth1_pan']")
	WebElement  PAN_GSTN;




	/* Locator NEXT on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addcomP5' and contains(text(),'Next')]")
	WebElement  Next_GSTN;


	/* Locator GSTR3B_march2018 of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP5']/div/div[2]/span/span/span[@class='k-select']")
	List <WebElement>  GSTR3B_List;

	/* Locator List Compile   GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//li[contains(text(),'Compile')]")
	List <WebElement>  GSTR3B_Compile;

	//div[@class='k-animation-container']/div[contains(@id,'editComp_3b_input_type')]
	/* Locator Next on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addcomP6' and contains(text(),'Next')]")
	WebElement Next_GSTR3B;

	//Company Add - Turnover 2016-2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2016-2017')]/parent::div/div/input[@id='addComp_avg_turnover']")
	WebElement comp_addturnover1;

	//Company Add - Turnover April-June 2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover April-June 2017')]/parent::div/div/input[@id='addComp_curr_turnover']")
	WebElement comp_addturnover2;

	//Company Add - Turnover 2017-2018
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2017-2018')]/parent::div/div/input[@id='addComp_avg_turnover1']")
	WebElement comp_addturnover3;

	//Company Edit - Turnover 2016-2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2016-2017')]/parent::div/div/input[@id='editComp_avg_turnover']")
	WebElement comp_editturnover1;

	//Company Edit - Turnover April-June 2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover April-June 2017')]/parent::div/div/input[@id='editComp_curr_turnover']")
	WebElement comp_editturnover2;

	//Company Edit - Turnover 2017-2018
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2017-2018')]/parent::div/div/input[@id='editComp_avg_turnover1']")
	WebElement comp_editturnover3;

	//Company View - Turnover 2016-2017
	@FindBy(how = How.XPATH,using=".//*[@id='viewComp_avg_turnover']")
	WebElement comp_viewturnover1;

	//Company View - Turnover April-June 2017
	@FindBy(how = How.XPATH,using=".//*[@id='viewComp_curr_turnover']")
	WebElement comp_viewturnover2;

	//Company View - Turnover 2017-2018
	@FindBy(how = How.XPATH,using=".//*[@id='viewComp_avg_turnover1']")
	WebElement comp_viewturnover3;
	
	/* Locator HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_out_HSN_config']")
	WebElement HSN_checkbox;

	/* Locator ITR checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_in_ITC_config']")
	WebElement ITC_checkbox;

	/* Locator Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='next-step addCompany k-button k-success float-right' and contains(text(),'Save')]")
	WebElement Save_AddCompany;

	/* Locator Company successfully created on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Company_Created;

	/* Locator Company successfully creation List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/span")
	List <WebElement> CompCreatnList;

	/* Locator  for VIEW Company List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/div/div/div[1]/a")
	List <WebElement> CompCreatnListView;

	/* Locator  for EDIT Company List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/div/div/div[2]/a")
	List <WebElement> CompCreatnListEdit;




	//Xpaths for View Company

	/* Locator to view AccountName/Group on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_account_name']")
	WebElement View_Acc_Grp;

	/* Locator to view CompanyName on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_name']")
	WebElement View_CompanyName;


	/*  Locator to view Turnover on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_avg_turnover']")
	WebElement View_turnover;


	/* Locator to view Email-ID on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_email']")
	WebElement View_emailid;


	/*Locator to view State on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_main_state']")
	WebElement View_State ;


	/* Locator to view GSTIN on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_gstin']")
	WebElement View_GSTIN ;

	/* Locator to view Pan on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_pan']")
	WebElement View_Pan ;

	/*Locator to view TurnoverApr-June on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_curr_turnover']")
	WebElement View_turnoverApr_June;

	/*Locator to view Mobile on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_mobile']")
	WebElement View_mobile;


	/* Locator to view Constitution on View Company page */
	@FindBy(how = How.XPATH, using=".//*[@id='viewComp_constitution']")
	WebElement View_Constitution;


	/* Locator to view City on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_main_city']")
	WebElement View_City;

	/* Locator to Address_Tab on View Company page*/
	@FindBy(how = How.XPATH, using=".//a[@id='comViewP3' and contains(text(),'Address')]")
	WebElement Address_Tab;



	/* Locator to view Address1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_add_line_1']")
	WebElement View_Add1;

	/* Locator to view Address2 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_add_line_2']")
	WebElement View_Add2;


	/*Locator to view Pincode on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_pincode']")
	WebElement View_Pincode;

	/*Locator to view Addresstype on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_address_type']")
	WebElement View_Addresstype;

	/*Locator to view Premises on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_premises']")
	WebElement View_Premises;

	/*Locator to view State on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_state']")
	WebElement View_StateAddress;

	/*Locator to view City on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_city']")
	WebElement View_CityAddress;

	/*Locator to view Nature of Business Activity on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_nature']")
	WebElement View_BA;

	/*Locator to click on GSTIN_User Tab on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//a[@id='comViewP4' and contains(text(),'GST User')]")
	WebElement View_GSTIN_User;


	/*Locator to view GSTIN_Username on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_username']")
	WebElement View_GSTIN_Username;


	/*Locator to view GSTIN_Username on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_email']")
	WebElement View_GSTIN_Email_ID;


	/* Locator Verify Name on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_name']")

	WebElement  View_Name_GSTN;

	/* Locator Verify Number on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_mobile']")
	WebElement  View_Number_GSTN;


	/* Locator Verify PAN on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_pan']")
	WebElement  View_PAN_GSTN;


	/* Locator GSTR3B on View Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='comViewP5' and contains(text(),'GSTR3B')]")
	WebElement  View_GSTRR3B;


	/* Locator List of GSTR3B Compile on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewComp_gstr3B_config']/div/div/span")
	List <WebElement>  List_GSTRR3BCompile;


	/* Locator List of GSTR3B Compile on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewComp_gstr3B_config']/div/div/span")
	List <WebElement>  List_GSTRR3BDirectInput;

	/* Locator Other Configuration on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='comViewP6' and contains(text(),'Other Configuration')]")
	WebElement  View_OtherConfig;

	/* Locator Type in Other Configuration on HSN Summary VALUE on View Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Upload HSN Summary Separately:')]/parent::div/span[@id='viewComp_out_HSN']")
	WebElement  View_HSNSummary_Value;


	/* Locator Type in Other Configuration on ITC VALUE on View Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Allow ITC Available Upload:')]/parent::div/span[@id='viewComp_in_itcUpload']")
	WebElement  View_ITC_Value;

	/* Locator CLOSE on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='compViewModal']/div/div/div/button[contains(text(),'Close')]")
	WebElement  View_Close;




	//Xpaths for EDIT Company

	/* Locator  for EDIT Company List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/div/div/div[2]/a")
	List <WebElement> CompListEdit;

	/*
	 * Sample
	 */
	/* Locator EDIT  Check non-editable Field - AccountGrp_Name Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_account_name' and @readonly='']")
	WebElement  Edit_AccGrp;


	/* Locator for EDIT CompanyName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_name' and @readonly='']")
	WebElement Edit_CompanyName;


	/* Locator for EDIT TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_avg_turnover']")
	WebElement Edit_TurnOver16_17;


	/* Locator for EDIT Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_email']")
	WebElement Edit_Registered_Email_id;

	/* Locator for EDIT  TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_tradename']")
	WebElement Edit_TradeName;

	/* Locator for EDIT Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='editcompP1']/div/div/div/div/span/span")
	WebElement  Edit_State_Present;

	/* Locator for EDIT GSTIN on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_gstin' and @class='k-textbox' and @type='text' and @readonly='']")
	WebElement Edit_GSTIN;


	/* Locator for EDIT PAN on  Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_pan' and @class='k-textbox' and  @type='text' and @readonly='']")
	WebElement Edit_PAN;

	/* Locator for EDIT  TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_curr_turnover']")
	WebElement Edit_TurnOver_AprilJune2017;

	/* Locator for EDIT Mobile  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_mobile' and @class='k-textbox' and @type='text']")
	WebElement Edit_Mobile;


	/* Locator for EDIT Select Constitution of Business  Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_constitution' and @readonly='']")
	WebElement  Edit_ConstOfBussiness;


	/* Locator for EDIT Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP1']/div[5]/div[2]/div/div/span/span/span[2]")
	WebElement  Edit_City_District_Dropdown;


	/* Locator for EDIT Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='editComp_main_city-list']/div[@class='k-list-scroller']/ul/li")
	List <WebElement>  Edit_ListofCity_District;


	/* Locator for Address on Edit Company Details on Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='editcomP3']")
	WebElement  Edit_Address_Tab;


	/* Locator EDIT Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_add_line_1']")
	WebElement  Edit_address1;

	/* Locator EDIT Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_add_line_2' and @class='clearEdit k-textbox']")
	WebElement  Edit_address2;

	/* Locator EDIT Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_pincode']")
	WebElement   Edit_pincode_address;


	/* Locator EDIT Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[1]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement   Edit_dropdown_address;


	/* Locator EDIT Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_address_type_listbox']/li")
	List <WebElement>   Edit_ListddlAddress;

	/* Locator EDIT Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[1]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement   Edit_dropdown_Premises;


	/* Locator EDIT Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_premises_listbox']/li")
	List <WebElement>   Edit_ListddlPremises;


	/* Locator EDIT States  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[4]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement   Edit_dropdown_States;


	/* Locator EDIT States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_state_listbox']/li")
	List <WebElement>   Edit_ListddlStates;

	/* Locator EDIT City  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[4]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement  Edit_dropdown_City;


	/* Locator EDIT City  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_city_listbox']/li")
	List <WebElement>  Edit_ListddlCity;

	/* Locator EDIT BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div[2]/span/span/span[@class='k-select']")
	WebElement  Edit_dropdown_BussinessActvty;


	/* Locator EDIT  BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_nature_listbox']/li")
	List <WebElement>  Edit_ListBussinessActvty;


	/* Locator EDIT  NEXT on  add address Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editcomP4' and contains(text(),'Next')]")
	WebElement  Edit_Address_next;



	/* Locator EDIT GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_username']")
	WebElement  Edit_GSTN_username;


	/* Locator EDIT GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='editcompP4']/div[2]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Edit_GSTN_username1;

	/* Locator EDIT GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='editcompP4']/div[3]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Edit_GSTN_username2;


	/* Locator EDIT GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_auth1_email_listbox']/li")
	List <WebElement>  Edit_GSTN_users;


	/* Locator EDIT Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editComp_auth1_name']")
	WebElement  Edit_Name_GSTN;

	/* Locator EDIT Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editComp_auth1_mobile']")
	WebElement  Edit_Number_GSTN;


	/* Locator EDIT Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editComp_auth1_pan']")
	WebElement  Edit_PAN_GSTN;


	/* Locator EDIT NEXT on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editcomP5' and contains(text(),'Next')]")
	WebElement  Edit_GSTUser_Next_GSTN;

	/* Locator Edit of List of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_gstr3B_config']/div/div[2]/span/span/span[2]")
	List <WebElement>  Edit_GSTR3B_List;

	/* Locator Edit of List Direct Input  of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//ul[contains(@id,'editComp_3b_input_type')]/li[contains(text(),'Direct Input')]")
	List <WebElement>  EditDirectInput_GSTR3B_List;

	/* Locator EDIT Next on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editcomP6' and contains(text(),'Next')]")
	WebElement  Edit_Next_GSTR3B;


	/* Locator EDIT HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_out_HSN_config']")
	WebElement Edit_HSN_checkbox;

	/* Locator EDIT ITC checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_in_ITC_config']")
	WebElement Edit_ITC_checkbox;

	/* Locator EDIT Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='next-step updateComp k-button k-success float-right' and contains(text(),'Save')]")
	WebElement Edit_Save_AddCompany;

	/* Locator Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@data-role='confirm' and contains(text(),'Company Updated successfully..')]")
	WebElement CompanyUpdated_Success;

	/* Locator for OK Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Edit_OK;

	//Xpaths for Location

	/* Locator for Location_1 page on Company page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Location')]")
	WebElement VerifyLocation;

	/* Locator for Add Location_1 on Company page  */
	@FindBy(how = How.XPATH, using="//button[@trigger-id='addsubP1' and @type='button']")
	WebElement AddLocation;


	//Xpaths for Adding a New Location_1 data from excel

	/* Locator for Location_1  List of Companies  for Location on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_company_name_listbox']/li")
	List <WebElement> ListOfCompanies;


	/* Locator for Location_1  Select  Company DropDown on Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[1]/div/div/div/span/span/span[@class='k-select']")
	WebElement SelectCompanyDDLforLoc1;

	/* Locator for Location_1  Legal Name of the Business on Location page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_name']")
	WebElement VerifyLegalBussinesName_Loc1;

	/* Locator for Location_1  TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_avg_turnover']")
	WebElement Loc1_TurnOver16_17;


	/* Locator for Location_1  Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_email']")
	WebElement Loc1_Registered_Email_id;

	/* Locator for Location_1  TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_tradename']")
	WebElement Loc1_TradeName;

	/* Locator for Location_1  Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[5]/div[1]/div/div/span/span/span[2]")
	WebElement  Loc1_State_Dropdown;


	/* Locator for Location_1   Select List of States from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_main_state_listbox']/li")
	List <WebElement>  Loc1_ListofStates;


	/* Locator for Location_1  GSTIN on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_gstin']")
	WebElement Loc1_GSTIN;


	/* Locator for Location_1  PAN on  Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_pan' and @readonly='']")
	WebElement Loc1_PAN;

	/* Locator for Location_1   TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_curr_turnover']")
	WebElement Loc1_TurnOver_AprilJune2017;

	/* Locator for Location_1   Mobile  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_mobile']")
	WebElement Loc1_Mobile;


	/* Locator for Location_1  of  Constitution of Business  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_constitution' and @readonly='']")
	WebElement  Loc1_ConstOfBussiness;


	/* Locator for Location_1  of Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[5]/div[2]/div/div/span/span/span[2]")
	WebElement  Loc1_City_District_Dropdown;


	/* Locator for Location_1  ocation of Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_main_city_listbox']/li")
	List <WebElement>  Loc1_ListofCity_District;

	/* Locator for Location_1  ocation of Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_main_city_listbox']/li")
	List <WebElement>  Loc2_ListofCity_District;


	/* Locator for Location_1  Address Tab on add a new location page  */
	@FindBy(how = How.XPATH, using=".//a[@id='addsubP3' and contains(text(),'Address')]")
	WebElement  Loc1_AddressTab;

	//Xpaths for Location_1 Address Tab for add new Location on Company Page

	/* Locator Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_add_line_1' and @class='clearEdit k-textbox']")
	WebElement  Loc1_address1;

	/* Locator Location Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_add_line_2' and @class='clearEdit k-textbox']")
	WebElement  Loc1_address2;

	/* Locator Location Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addLoc_pincode']")
	WebElement  Loc1_pincode_address;

	/* Locator Location Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[1]/div[1]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_address;


	/* Locator Location Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_address_type_listbox']/li")
	List <WebElement>  Loc1_ListddlAddress;


	/* Locator Location Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[1]/div[2]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_Premises;


	/* Locator Location  Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_premises_listbox']/li")
	List <WebElement>  Loc1_ListddlPremises;


	/* Locator Location States dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[4]/div[1]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_States;


	/* Locator Location States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_state_listbox']/li")
	List <WebElement>  Loc1_ListddlStates;

	/* Locator Location City dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[4]/div[2]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_City;


	/* Locator Location  City List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_city_listbox']/li")
	List <WebElement>  Loc1_ListddlCity;

	/* Locator Location BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP3']/div[2]/div[2]/span/span/span[2]")
	WebElement  Loc1_dropdown_BussinessActvty;


	/* Locator  Location BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_nature_listbox']/li")
	List <WebElement>  Loc1_ListBussinessActvty;


	/* Locator Location Next_Adress on add address Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='prev2' and contains(text(),'Next')]")
	WebElement  Loc1_Next_to_GST_User;


	/* Locator Location GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_username']")
	WebElement  Loc1_GSTN_username;


	/* Locator Location GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addsublocP4']/div[2]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Loc1_GSTN_username1;

	/* Locator Location GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addsublocP4']/div[3]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Loc1_GSTN_username2;


	/* Locator Location GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_auth1_email_listbox']/li")
	List <WebElement>  Loc1_GSTN_users;


	/* Locator Location Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addLoc_auth1_name']")
	WebElement  Loc1_Name_GSTN;

	/* Locator Location Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='addLoc_auth1_mobile']")
	WebElement  Loc1_Number_GSTN;


	/* Locator Location Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addLoc_auth1_pan']")
	WebElement  Loc1_PAN_GSTN;


	/* Locator Location 1 on NEXT  GST User on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addsubP5' and contains(text(),'Next')]")
	WebElement  Loc1_Next_GSTR3B;


	/* Locator GSTR3B _Loc01 of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP5']/div/div[2]/span/span/span[2]")
	List <WebElement>  GSTR3B_Loc01_List;


	/* Locator List Compile Loc01  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//li[contains(text(),'Compile')]")
	List <WebElement>  GSTR3B_Loc01_Compile;


	/* Locator Location 1 on NEXT on GSTR3B Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addsubP6' and contains(text(),'Next')]")
	WebElement  Loc1_Next_HSN;


	/* Locator  Location 1  dropdown on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP5']/div/div[2]/span/span/span[@class='k-select']")
	WebElement Loc1_GSTR3B;


	/* Locator  Location 1  GSTR3B_march2018 of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_3b_input_type0_listbox']/li")
	List <WebElement>  Loc1_GSTR3B_march2018;


	/* Locator  Location 1  Next on GSTR3B tab on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP5']/div/div[2]/span/span/span[@class='k-select']")
	WebElement Loc1_HSN_Next;


	/* Locator of Location 1  HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_out_HSN_config']")
	WebElement Loc1_HSN_checkbox;

	/* Locator of  Location 1 ITR checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_in_ITC_config']")
	WebElement Loc1_ITC_checkbox;

	/* Locator of  Location 1 Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button addLoc k-success float-right' and contains(text(),'Save')]")
	WebElement Save_AddLoc_1;


	/* Locator of  Location 1 successfully created on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Location_1_Created;

	/* Locator Location_1 successfully creation List on Company page  */
	@FindBy(how = How.XPATH, using="//div[@class='card-block ov-auto sublocappend']/div/span")
	List <WebElement> Loc_1_CreatnList;


	//Xapths for View Location

	/* Locator Location 1 for VIEW Loc_1 List on Company page  */
	@FindBy(how = How.XPATH, using="//div[@class='card-block ov-auto sublocappend']/div/div/div/div[1]")
	List <WebElement> Loc_1CreatnListView;

	/* Locator Location 1 for VIEW Loc_1 List on Company page  */
	@FindBy(how = How.XPATH, using="//div[@class='card-block ov-auto sublocappend']/div/div/div/div[2]")
	List <WebElement> Loc_1CreatnListEdit;

	/* Locator Location 1  to view Company Loc_1 on View Location page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_company_name']")
	WebElement View_Loc1_Company;

	/* Locator Location 1  to view Legal Name of Bussiness Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_name']")
	WebElement View_Loc1_bussiness;


	/*  Locator Location 1  to view Turnover Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_avg_turnover']")
	WebElement View_Loc1_turnover;


	/* Locator  Location 1  to view Email-ID Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_email']")
	WebElement View_Loc1_emailid;


	/* Locator Location 1  to view TradeName Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_tradename']")
	WebElement View_Loc1_Tradename;


	/*Locator Location 1  to view State Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_main_state']")
	WebElement View_Loc1_State ;


	/* Locator Location 1  to view GSTIN Loc_1 on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_gstin']")
	WebElement View_GSTIN_Loc1_State ;

	/* Locator Location 1  to view Pan Loc_1 on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_pan']")
	WebElement View_Pan_Loc1_State ;

	/*Locator Location 1 to view TurnoverApr-June Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//*[@id='viewLoc_curr_turnover']")
	WebElement View_turnoverApr_Loc1_June;

	/*Locator Location 1 to view Mobile Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_mobile']")
	WebElement View_Loc1_mobile;


	/* Locator Location 1 to view Constitution Loc_1 on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_constitution']")
	WebElement View_Loc1_Constitution;


	/* Locator Location 1  to view City Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_main_city']")
	WebElement View_Loc1_City;

	/* Locator Location 1  to Address_Tab Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//a[@id='locViewP3']")
	WebElement View_Address_Loc1_Tab;



	/* Locator Location 1  to view Address1 Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_add_line_1']")
	WebElement View_Add1_Loc_1;

	/* Locator Location 1  to view Address2 Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_add_line_2']")
	WebElement View_Add2_Loc_1;


	/*Locator Location 1   to view Pincode Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_pincode']")
	WebElement View_Pincode_Loc_1;

	/*Locator Location 1  to view Addresstype Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_address_type']")
	WebElement View_Addresstype_Loc_1;

	/*Locator Location 1  to view Premises Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_premises']")
	WebElement View_Premises_Loc_1;

	/*Locator Location 1  to view State Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_state']")
	WebElement View_StateAddress_Loc_1;

	/*Locator Location 1  to view City Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_city']")
	WebElement View_CityAddress_Loc_1;

	/*Locator Location 1  to view Nature of Business Activity Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_nature']")
	WebElement View_BA_Loc_1;

	/*Locator Location 1  to click on GSTIN_User Tab Loc_1 on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using="//a[@id='locViewP4' and contains(text(),'GST User')]")
	WebElement View_GSTIN_User_Loc_1;


	/*Locator Location 1  to view GSTIN_Username Loc_1 on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_username']")
	WebElement View_GSTIN_Username_Loc_1;


	/*Locator Location 1  to view GSTIN_Username Loc_1 on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_email']")
	WebElement View_GSTIN_Email_ID_Loc_1;


	/* Locator Location 1  Verify Name Loc_1 on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_name']")
	WebElement  View_Name_GSTN_Loc_1;

	/* Locator Location 1  Verify Number Loc_1 on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_mobile']")
	WebElement  View_Number_GSTN_Loc_1;


	/* Locator Location 1  Verify PAN Loc_1 son  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_pan']")
	WebElement  View_PAN_GSTN_Loc_1;


	/* Locator Location 1  GSTR3B on View Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='locViewP5' and contains(text(),'GSTR3B')]")
	WebElement  View_GSTRR3B_Loc_1;

	/* Locator List of Location 1 GSTR3B Compile on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewLoc_gstr3B_config']/div/div[2]/span")
	List <WebElement>  List_Loc_GSTRR3BCompile;


	/* Locator Location 1  Type in GSTR3B on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewLoc_gstr3B_config']/div/div[2]/span")
	WebElement  View_Type_Loc_1;


	//View X-paths for HSN & ITC


	/* Locator Location 1  Other Configuration on View Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='locViewP6' and contains(text(),'Other Configuration')]")
	WebElement  Loc_1_View_OtherConfig;


	/* Locator Location 1  Type in Other Configuration on HSN Summary VALUE on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_out_HSN_config']")
	WebElement   Loc_1_View_HSNSummary_Value;


	/* Locator Location 1  Type in Other Configuration on ITC VALUE on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_in_ITC_config']")
	WebElement  Loc_1_View_ITC_Value;

	/* Locator Location 1 CLOSE on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='sublocViewModal']/div/div/div/button[contains(text(),'Close')]")
	WebElement   Loc_1_View_Close;


	//Xpaths for EDIT
	/*
	 * Sample
	 *
	/* Locator Location EDIT  Check non-editable Field - Loc_1_Edit_CompanyName Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_company_name' and @readonly='']")
	WebElement  Loc_1_Edit_CompanyName;


	/* Locator for EDIT Legal NameofBussiness on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_name' and @readonly='']")
	WebElement Loc_1_Edit_NameofBussiness;


	/* Locator for EDIT TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_avg_turnover']")
	WebElement Loc_1_Edit_TurnOver16_17;


	/* Locator for EDIT Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_email']")
	WebElement Loc_1_Edit_Registered_Email_id;


	/* Locator for EDIT  TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_tradename']")
	WebElement Loc_1_Edit_TradeName;


	/* Locator for EDIT Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP1']/div[5]/div[1]/div/div/span/span/span[1]")
	WebElement  Loc_1_Edit_State_Present;


	/* Locator for EDIT GSTIN on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editLoc_gstin' and @class='k-textbox' and @type='text' and @readonly='']")
	WebElement Loc_1_Edit_GSTIN;


	/* Locator for EDIT PAN on  Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editLoc_pan' and @class='k-textbox' and  @type='text' and @readonly='']")
	WebElement Loc_1_Edit_PAN;

	/* Locator for EDIT  TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_curr_turnover']")
	WebElement Loc_1_Edit_TurnOver_AprilJune2017;

	/* Locator for EDIT Mobile  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editLoc_mobile' and @class='k-textbox' and @type='text']")
	WebElement Loc_1_Edit_Mobile;


	/* Locator for EDIT Select Constitution of Business  Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_constitution' and @readonly='']")
	WebElement  Loc_1_Edit_ConstOfBussiness;


	/* Locator for EDIT Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP1']/div[5]/div[2]/div/div/span/span/span[2]")
	WebElement  Loc_1_Edit_City_District_Dropdown;


	/* Locator for EDIT Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_main_city_listbox']/li")
	List <WebElement>   Loc_1_Edit_ListofCity_District;


	/* Locator for Address on Edit Company Details on Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='editsubP3']")
	WebElement  Loc_1_Edit_Address_Tab;


	/* Locator EDIT Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_add_line_1']")
	WebElement  Loc_1_Edit_address1;

	/* Locator EDIT Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_add_line_2']")
	WebElement  Loc_1_Edit_address2;

	/* Locator EDIT Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_pincode']")
	WebElement   Loc_1_Edit_pincode_address;


	/* Locator EDIT Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[1]/div[1]/div/div/span/span/span[2]")
	WebElement   Loc_1_Edit_dropdown_address;


	/* Locator EDIT Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_address_type_listbox']/li")
	List <WebElement>  Loc_1_Edit_ListddlAddress;

	/* Locator EDIT Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[1]/div[2]/div/div/span/span/span[2]")
	WebElement   Loc_1_Edit_dropdown_Premises;


	/* Locator EDIT Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_premises_listbox']/li")
	List <WebElement>   Loc_1_Edit_ListddlPremises;


	/* Locator EDIT States  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[4]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement    Loc_1_Edit_dropdown_States;


	/* Locator EDIT States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_state_listbox']/li	")
	List <WebElement>    Loc_1_Edit_ListddlStates;

	/* Locator EDIT City  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[4]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement   Loc_1_Edit_dropdown_City;


	/* Locator EDIT City  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_city_listbox']/li")
	List <WebElement>   Loc_1_Edit_ListddlCity;

	/* Locator EDIT BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[2]/div[2]/span/span/span[@class='k-select']")
	WebElement  Loc_1_Edit_dropdown_BussinessActvty;


	/* Locator EDIT  BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_nature_listbox']/li")
	List <WebElement>  Loc_1_Edit_ListBussinessActvty;


	/* Locator EDIT  NEXT on  add address Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editsubP4' and contains(text(),'Next')]")
	WebElement  Loc_1_Edit_Address_next;



	/* Locator EDIT GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_username']")
	WebElement  Loc_1_Edit_GSTN_username;


	/* Locator EDIT GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP4']/div[2]/div[1]/span/span/span[2]")
	WebElement  Loc_1_Edit_GSTN_username1;

	/* Locator EDIT GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP4']/div[3]/div[1]/span/span/span[2]")
	WebElement  Loc_1_Edit_GSTN_username2;


	/* Locator EDIT GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_auth1_email_listbox']/li")
	List <WebElement>  Loc_1_Edit_GSTN_users;


	/* Locator EDIT Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='editLoc_auth1_name']")
	WebElement  Loc_1_Edit_Name_GSTN;

	/* Locator EDIT Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editLoc_auth1_mobile']")
	WebElement  Loc_1_Edit_Number_GSTN;


	/* Locator EDIT Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editLoc_auth1_pan']")
	WebElement  Loc_1_Edit_PAN_GSTN;


	/* Locator EDIT NEXT on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editsubP5' and contains(text(),'Next')]")
	WebElement   Loc_1_Edit_GSTUser_Next_GSTN;




	/* Locator EDIT GSTR3B DropDown of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_gstr3B_config']/div/div[2]/span/span/span[2]")
	List <WebElement>  Loc_1_Edit_GSTR3B_drpdown;



	/* Locator EDIT GSTR3B Direct Input of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//li[contains(text(),'Direct Input')]")
	List <WebElement>  Loc_1_Edit_GSTR3B_DirectInput;



	/* Locator EDIT Next on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editsubP6' and contains(text(),'Next')]")
	WebElement  Loc_1_Edit_Next_GSTR3B;


	/* Locator EDIT HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_out_HSN_config']")
	WebElement Loc_1_Edit_HSN_checkbox;

	/* Locator EDIT ITC checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_in_ITC_config']")
	WebElement Loc_1_Edit_ITC_checkbox;

	/* Locator EDIT Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button updateLoc k-success float-right' and contains(text(),'Save')]")
	WebElement Loc_1_Edit_Save_AddCompany;

	/* Locator Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@data-role='confirm' and contains(text(),'Location Updated successfully.')]")
	WebElement Loc_1_LocationUpdated_Success;

	/* Locator for OK Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Loc_1_Edit_OK;




	//Negative scenaios for Company 1
	/* Locator for Please Select State on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_main_state-list']/div[contains(text(),'Please Select')]")
	WebElement Comp01_pleaseSelect_State;


	/* Locator for Please Select State on constitution of bussiness on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_constitution-list']/div[contains(text(),'Please Select')]")
	WebElement Comp01_pleaseSelect_ConstBussiness;


	/* Locator for Please Select City on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_constitution-list']/div[contains(text(),'Please Select')]")
	WebElement Comp01_pleaseSelect_City;


	//Xpaths for GSTR3B tab 5 months present

	/* Locator for first GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_1;


	/* Locator for second GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_2;


	/* Locator for third GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_3;


	/* Locator for fourth GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_4;


	/* Locator for fifth GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_5;

	//xpath for _indicates error on save

	/* Locator for Save_indicates Error Company on   Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP6']/span[@class='errindi']")
	WebElement Save_indicatesError_comp;


	/* Locator for Save_indicates Error Location on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP6']/span[@class='errindi']")
	WebElement Save_indicatesError_Loc01;


	//xpaths for errors on subtabs on Add Company

	/* Locator for error1  on Basic Information Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error1']")
	WebElement error_1;

	/* Locator for error3  on Address Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error3']")
	WebElement error_3;


	/* Locator for error4  on GST User Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error4']")
	WebElement error_4;


	/* Locator for error5  on GSTR3B Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error5']")
	WebElement error_5;

	/* Locator for error6  on GSTR3B Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error6']")
	WebElement error_6;
	

	/* Locator for error msg for company name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Company Name')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement CompName_ErrorMsg;

	/* Locator for error msg for EMail name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Email-id')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Email_ErrorMsg;


	/* Locator for error msg for Trade name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Trade Name/Alias')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement TradeName_ErrorMsg;


	/* Locator for error msg for State name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'State')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement State_ErrorMsg;


	/* Locator for error msg for GSTIN_ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'GSTIN ')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement GSTIN_ErrorMsg;


	/* Locator for error msg for PAN_ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'PAN')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement PAN_ErrorMsg;


	/* Locator for error msg for Turnover name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover 2016-2017')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Turnover_ErrorMsg1;

	
	/* Locator for error msg for Turnover_2017__ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover April-June 2017')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Turnover_ErrorMsg2;


	/* Locator for error msg for Turnover_2017__ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover 2017-2018')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Turnover_ErrorMsg3;
	
	
	/* Locator for error msg for Registered Mobile on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Mobile')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Registered_Mobile_ErrorMsg;


	/* Locator for error msg for ConstBussiness on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Constitution of Business')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement ConstBussiness_ErrorMsg;	

	//xpaths for address sub tab
	/* Locator for AddLine1 on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 1')]/span[@data-for='addComp_add_line_1']")
	WebElement AddLine1_ErrorMsg;	


	/* Locator for AddLine2 on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 2')]/span[@data-for='addComp_add_line_2']")
	WebElement AddLine2_ErrorMsg;


	/* Locator for  AddressType on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Type')]/span[@data-for='addComp_address_type']")
	WebElement AddressType_ErrorMsg;


	/* Locator for PinCode on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'PIN Code')]/span[@data-for='addComp_pincode']")
	WebElement PinCode_ErrorMsg;


	/* Locator for Add State on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'State')]/span[@data-for='addComp_state']")
	WebElement AddState_ErrorMsg;


	/* Locator for Nature of BA on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Nature of Business Activity')]/span[@data-for='addComp_nature']")
	WebElement NatureofBA_ErrorMsg;


	/* Locator for GSTIN Username  on GST User Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'GSTIN Username')]/span[@data-for='addComp_username']")
	WebElement GSTIN_Username_ErrorMsg;


	/* Locator for List  of months in GSTR3B  on GSTR3B Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@data-for='addComp_3b_input_type[]']")
	List <WebElement> List_months_GSTR3B_ErrorMsg;



	//xpaths for errors on SubTabs of Add Location


	/* Locator for error_01_Loc1  on Basic Information Sub-tab  Add Location page  */

	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab1']")
	WebElement error_1_LOC01;


	/* Locator for error_03_Loc1   on Address Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab3']")
	WebElement error_3_LOC01;


	/* Locator for error_04_Loc1   on GST User Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab4']")
	WebElement error_4_LOC01;


	/* Locator for error_05_Loc1   on GSTR3B Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab5']")
	WebElement error_5_LOC01;


	/* Locator for error_06_Loc1   on GSTR3B Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab6']")
	WebElement error_6_LOC01;
	
	/* Locator for error msg for Turnover name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover 2016-2017')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Loc_Turnover_ErrorMsg1;

	
	/* Locator for error msg for Turnover_2017__ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover April-June 2017')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Loc_Turnover_ErrorMsg2;


	/* Locator for error msg for Turnover_2017__ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover 2017-2018')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Loc_Turnover_ErrorMsg3;

	/* Locator for loc1 please select option in state dropdown   on GSTR3B Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using="//div[@id='addLoc_main_state-list']/div[contains(text(),'Please Select')]")
	WebElement Loc01_State_PleaseSelect;


	/* Locator for Select City Dropdown on Location 01 page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[6]/div[2]/div/div/span/span[@class='k-dropdown-wrap k-state-disabled']")
	WebElement  Loc01_CityDropdown_stateinactive;



	/* Locator for error msg for company name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Select Company')]/span[@data-for='addLoc_company_name']")
	WebElement Loc01_Company_MandatoryMsg;


	/* Locator for error msg for company name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover 2016-2017')]/span[@data-for='addLoc_avg_turnover']")
	WebElement Loc01_Turnover_ErrorMsg;


	/* Locator for error msg for Turnover name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Email-id')]/span[@data-for='addLoc_email']")
	WebElement Loc01_Email_ErrorMsg;


	/* Locator for error msg for EMail name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Trade Name/Alias')]/span[@data-for='addLoc_tradename']")
	WebElement Loc01_TradeName_ErrorMsg;


	/* Locator for error msg for Trade name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'State')]/span[@data-for='addLoc_main_state']")
	WebElement Loc01_State_ErrorMsg;


	/* Locator for error msg for State name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'GSTIN')]/span[@data-for='addLoc_gstin']")
	WebElement Loc01_GSTIN_ErrorMsg;


	/* Locator for error msg for GSTIN_ErrorMsg on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover April-June 2017')]/span[@data-for='addLoc_curr_turnover']")
	WebElement Loc01_TurnoverApril2017_ErrorMsg;


	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Mobile')]/span[@data-for='addLoc_mobile']")
	WebElement Loc01_RegisteredMobile_ErrorMsg;




	//xpaths for address sub tab

	/* Locator for AddLine1 on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 1')]/span[@data-for='addLoc_add_line_1']")
	WebElement AddLine1_Loc01_ErrorMsg;	


	/* Locator for AddLine2 on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 2')]/span[@data-for='addLoc_add_line_2']")
	WebElement AddLine2_Loc01_ErrorMsg;


	/* Locator for  AddressType on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Type')]/span[@data-for='addLoc_address_type']")
	WebElement AddressType_Loc01_ErrorMsg;


	/* Locator for PinCode on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'PIN Code')]/span[@data-for='addLoc_pincode']")
	WebElement PinCode_Loc01_ErrorMsg;


	/* Locator for Add State on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'State')]/span[@data-for='addLoc_state']")
	WebElement AddState_Loc01_ErrorMsg;


	/* Locator for Nature of BA on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Nature of Business Activity')]/span[@data-for='addLoc_nature']")
	WebElement NatureofBA_Loc01_ErrorMsg;


	/* Locator for GSTIN Username  on GST User Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using=".//p[@id='chCompName']/span[@data-for='addLoc_username']")
	WebElement GSTIN_Username_Loc01_ErrorMsg;


	/* Locator for List  of months in GSTR3B  on GSTR3B Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//span[@data-for='addLoc_3b_input_type[]']")
	List <WebElement> List_months_Loc01_GSTR3B_ErrorMsg;

	/* Locator for Other Configuration on Sub-tab of Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcomP6']")
	WebElement OtherConfig_Comp_Neg_02;


	/* Locator for Other Configuration on Sub-tab of Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubP6']")
	WebElement OtherConfig_Loc_Neg_02;


	/* Locator for PAN exists error message on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div[@class='error_messages']")
	WebElement Comp01_Pan_exist_error;



	//Xpaths for Users

	/* Locator for user from the list on the dashboard page  */
	@FindBy(how = How.XPATH, using="//*[@id='menu']/li/a[contains(text(),'User')]")
	WebElement User;

	/* Locator for verify User page on Company page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Users')]")
	WebElement Verifyuser;

	/* Locator for TRP Users on Company page  */
	@FindBy(how = How.XPATH, using="//a[contains(text(),'TRP Users')]")
	WebElement TRPUsers;

	/* Locator for Add TRP Users on Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='openaddTRPUser k-button k-success m-r-0-imp float-right clear_gstins']")
	WebElement AddTRPUsers;

	/* Locator for list of client admin Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[4]")
	List <WebElement> ListClientadminUsers;

	/* Locator for list of client admin Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[7]/div/a[1]/i")
	List <WebElement> List_View_ClientadminUsers;

	/* Locator for Tradename in  Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='view_client_alloc']/tr/td[1]")
	WebElement User_TradeName;




	/* Locator for Home on Home page  */
	@FindBy(how = How.XPATH, using=".//li[@id='/home' and @class='nav-item Home active']/a")
	WebElement Home;
	
	/* Locator for Home After Utilitieson Home page  */
	@FindBy(how = How.XPATH, using="//div[@id='navbarNav']/ul[@class='navbar-nav border-bot float-left']/li/a[contains(text(),'Home')]")
	WebElement UtilityHome;
	
	
	/* Locator for Outward Supply on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li/a[contains(text(),'Outward Supply')]")
	WebElement OutwardSupply;

	/* Locator for Inward Supply on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li/a[contains(text(),'Inward Supply')]")
	WebElement InwardSupply;

	/* Locator for GSTR3B on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li/a[contains(text(),'GSTR3B')]")
	WebElement GSTR3B;

	/* Locator for Returns on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li/a[contains(text(),'Returns')]")
	WebElement Returns;

	/* Locator for Ledgers on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li/a[contains(text(),'Ledgers')]")
	WebElement Ledgers;

	/* Locator for Reports on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li/a[contains(text(),'Reports')]")
	WebElement Reports;

	/* Locator for Utilities on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li[@class='nav-item dropdown']/span[contains(text(),'Utilities')]")
	WebElement Utilities;

	/* Locator for Utilities DDL CLOSE on Home page  */
	@FindBy(how = How.XPATH, using="//li[@class='nav-item dropdown show']/span")
	WebElement UtilitiesDDL_Close;
	
	
	/* Locator for Customer Master on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li[@class='nav-item dropdown']/div/a[contains(text(),'Customer Master')]")
	WebElement Customer_Master;

	/* Locator for Vendor Master on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li[@class='nav-item dropdown']/div/a[contains(text(),'Vendor Master')]")
	WebElement Vendor_Master;

	/* Locator for HSN Item Master on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li[@class='nav-item dropdown']/div/a[contains(text(),'HSN Item Master')]")
	WebElement HSN_Item_Master;

	/* Locator for Invoice Print on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li[@class='nav-item dropdown']/div/a[contains(text(),'Invoice Print')]")
	WebElement Invoice_Print;

	/* Locator for Search HSN on Home page  */
	@FindBy(how = How.XPATH, using="//nav[@id='secnav']/div/ul/li[@class='nav-item dropdown']/div/a[contains(text(),'Search HSN')]")
	WebElement Search_HSN;

	/* Locator for Outward TradeName Dropdown on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[1]/span/span/span[2]")
	WebElement OutwardTradeNameDDL;


	/* Locator for Outward TradeName Values in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//div[@id='outwardGstin-list']/div[@class='k-list-scroller']/ul/li")
	List <WebElement> OutwardTradeNameList;

	
	/* Locator for Outward Calender on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[2]/span/span/span/span")
	WebElement Calender;

	/* Locator for Outward Month_Calender in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//div[@id='datePicker_dateview']/div/table/tbody/tr[1]/td[1]/a")
	WebElement Month_Calender;


	/* Locator for SaveGSTR1 on Home page  */
	@FindBy(how = How.XPATH, using="//a[@messagelog='Saved GSTR1' and contains(@class,'disabled')]")
	WebElement SaveGSTR1;

	/* Locator for Import File on Home page  */
	@FindBy(how = How.XPATH, using="//a[@messagelog='Imported Outward Data' and contains(@class,'disabled')]")
	WebElement ImportFile;


	/* Locator for Credit_Debit_Note on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardAmendments' and contains(text(),'Credit / Debit Note')]")
	WebElement Credit_Debit_Note;

	/* Locator for Tax On Advance on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardTOA' and contains(text(),'Tax On Advance')]")
	WebElement Tax_On_Advance;

	/* Locator for Doc Detail on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardDOC' and contains(text(),'Doc Detail')]")
	WebElement Doc_Detail;


	/* Locator for Amendments DropDown on Home page  */
	@FindBy(how = How.XPATH, using="//li/span[contains(text(),'Amendments')]")
	WebElement AmendmentsDDL;

	/* Locator for Amendments Invoice on Home page  */
	@FindBy(how = How.XPATH, using="//a[@id='invoicegstr1A']")
	WebElement AmendmentsInvoice;


	/* Locator for Amendments Credit / Debit Note on Home page  */
	@FindBy(how = How.XPATH, using=".//a[@id='amendgstr1A']")
	WebElement Amendments_Credit_Debit_Note;


	/* Locator for Inward  Credit / Debit Note on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='inwardAmendments' and contains(text(),'Credit / Debit Note')]")
	WebElement Inward_Credit_Debit_Note;

	/* Locator for Inward Tax Liability  on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='inwardTaxLiability' and contains(text(),'Tax Liability')]")
	WebElement Inward_Tax_Liability;

	/* Locator for Inward Tax Liability  on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='inwardITCReversal' and contains(text(),'ITC Reversal')]")
	WebElement Inward_ITC_Reversal;


	/* Locator for Inward ISD Credit  on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='inwardISDCredit' and contains(text(),'ISD Credit')]")
	WebElement Inward_ISD_Credit;

	/* Locator for Inward ITC Available DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[8]/span[contains(text(),'ITC Available')]")
	WebElement Inward_ITC_AvailableDDL;

	/* Locator for Inward ITC Available Invoice on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='inInvoiceITCAvail']")
	WebElement Inward_ITC_Available_Invoice;

	/* Locator for Inward ITC Available Credit/Debit Note on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='inAmendITCAvail']")
	WebElement Inward_ITC_Available_CreditDebit;


	/* Locator for Inward GSTR2A DDL on Home page  */
	@FindBy(how = How.XPATH, using="//li/span[contains(text(),'GSTR2A')]")
	WebElement Inward_GSTR2ADDL;

	/* Locator for Inward GSTR2A Invoice on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='invoicegstr2a']")
	WebElement Inward_GSTR2A_Invoice;

	/* Locator for Inward GSTR2A Credit/Debit Note on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='amendgstr2a']")
	WebElement Inward_GSTR2A_CreditDebit;

	/* Locator for Inward TradeName Values in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//ul[@id='inwardGstin_listbox']/li")
	List <WebElement> InwardTradeNameList;

	/* Locator for SaveGSTR1 on Home page  */
	@FindBy(how = How.XPATH, using="//a[@messagelog='Inward Submit GSTR2' and contains(@class,'disabled')]")
	WebElement SaveGSTR2_Inward;

	/* Locator for Import File on Home page  */
	@FindBy(how = How.XPATH, using="//a[@messagelog='Imported Inward Data' and contains(@class,'disabled')]")
	WebElement ImportFile_Inward;

	/* Locator for Import Get GSTR2A on Home page  */
	@FindBy(how = How.XPATH, using="//a[@messagelog='Inward Receive GSTR2A' and contains(@class,'disabled')]")
	WebElement GetGSTR2A_Inward;

	/* Locator for GSTR3B TradeName Dropdown on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[1]/span/span/span[2]")
	WebElement GSTR3BTradeNameDDL;

	/* Locator for GSTR3B TradeName Values in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='GSTINselection_listbox']/li")
	WebElement GSTR3BTradeNameList;

	/* Locator for GSTR3B Calender on Home page */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[2]/span/span/span/span[2]")
	WebElement GSTR3BCalender;

	/* Locator for GSTR3Bs Month_Calender  FIRST MONTH in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//table[@class='k-content k-meta-view k-year']/tbody/tr[1]/td[1]")
	WebElement GSTR3B_Month_Calender_First;

	/* Locator for GSTR3Bs Month_Calender  SECOND MONTH in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//table[@class='k-content k-meta-view k-year']/tbody/tr[1]/td[2]")
	WebElement GSTR3B_Month_Calender_Second;

	/* Locator for GSTR3Bs Compiled in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//a[@id='gstr3bComp' and contains(@class,'nav-link doc')]")
	WebElement GSTR3B_Compiled_FirstMonth;

	/* Locator for GSTR3Bs Input in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//a[@id='inputConf' and contains(@class,'nav-link doc disabled')] ")
	WebElement GSTR3B_Input_FirstMonth;


	/* Locator for GSTR3Bs Compiled in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//a[@id='gstr3bComp' and contains(@class,'disabled')]")
	WebElement GSTR3B_Compiled_SecondMonth;

	/* Locator for GSTR3Bs Input in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//a[@id='inputConf' and contains(@class,'nav-link doc')]")
	WebElement GSTR3B_Input_SecondMonth;

	/* Locator for GSTR3Bs ProcessData in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//form[@id='FormNameStoreCompiled']/div/a[contains(@class,'disabled')]")
	WebElement GSTR3B_ProcessData;

	/* Locator for GSTR3Bs SUBMIT TO GSTN in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//button[@id='submit_compiled_gstn' and contains(@class,'disabled')]")
	WebElement GSTR3B_SubmitoGSTN;

	/* Locator for GSTR3Bs Pending in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//button[@id='pending_compiled' and contains(@class,'disabled')]")
	WebElement GSTR3B_Pending;

	/* Locator for GSTR3Bs SendforApproval in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//button[@id='send_compiled' and contains(@class,'disabled')]")
	WebElement GSTR3B_SendforApproval;

	/* Locator for GSTR3Bs Save GSTN in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//button[@id='compiled_to_gstn' and contains(@class,'disabled')]")
	WebElement GSTR3B_SaveToGSTN;

	/* Locator for GSTR3Bs EnableGST in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//button[@id='compiled_to_enablegst' and contains(@class,'disabled')]")
	WebElement Save_to_EnableGST;

	/* Locator for Return Send for approval in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td[3]/button[contains(@class,'disabled')]")
	List <WebElement> Return_SendForApproval;

	/* Locator for Return Submit in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td[3]/button[contains(@class,'disabled')]")
	List <WebElement> Return_Submit;

	/* Locator for Return File in DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td[3]/button[contains(@class,'disabled')]")
	List <WebElement> Return_File;

	/* Locator for Returns TradeName  DDL on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div/div/div[1]/span/span/span[2]")
	WebElement ReturnTradeNameDDL;

	/* Locator for Returns TradeName Values in DDL on Home page  */
	@FindBy(how = How.XPATH, using="//ul[@id='returnGstin_listbox']/li")
	List <WebElement> ReturnTradeNameList;

	/* Locator for Returns Calender on Home page  */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div/div/div[2]/div/div[1]/span/span/span/span[2]")
	WebElement ReturnCalender;

	/* Locator for Returns Month on Home page  */
	@FindBy(how = How.XPATH, using="//table[@class='k-content k-meta-view k-year']/tbody/tr[1]/td[1]/a")
	WebElement ReturnMonth;

	/* Locator forUtilities DDL on Home page  */
	@FindBy(how = How.XPATH, using="//li[@class='nav-item dropdown show']/div/a")
	List <WebElement> UtilitiesDDL;

	
	/* Locator InvitesNo DDL on Home page  */
	@FindBy(how = How.XPATH, using="//a[@grid-id='receivedinvites_grid' and contains(@data-toggle,'tab')]")
	WebElement Invites;
	

	/* Locator View Account/Group Name on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='entity_name']")
	WebElement GroupName;
	
	/* Locator View Account/Group on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='entity_id']/div/a/i")
	WebElement ViewGroup;
	
	
	/* Locator View Email-ID on Account/Group Name on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='view_account_email']")
	WebElement GroupEmail_ID;
	
	/* Locator View Mobile Number on Account/Group Name on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='view_account_mobile']")
	WebElement GroupMobileNum;
	
	/* Locator View TRP on TRP Login page  */
	@FindBy(how = How.XPATH, using="//table[@role='treegrid']/tbody/tr/td/span")
	List <WebElement> View_TRP;
	
	/* Locator Select Group on TRP page  */
	@FindBy(how = How.XPATH, using=".//*[@id='groupSelectionGrid']/div[2]/table/tbody/tr/td[2]/a")
	WebElement SelectGroup;
	
	/* Locator Close Group on TRP page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewClient']/div/div/div[1]/button")
	WebElement CloseGroup;
	
	//for TRP
	
	/* Locator  on TRP page  */
	@FindBy(how = How.XPATH, using="//a[contains(text(),'TRP Users')]")
	WebElement TRP_Users;
	
	/* Locator for list of client admin Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='trpuserGrid']/tbody/tr/td[4]")
	List <WebElement> ListTRpUsers;

	/* Locator for list of client admin Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='trpuserGrid']/tbody/tr/td[5]/div/a[1]/i")
	List <WebElement> List_View_TRPUsers;

	/* Locator for Tradename in  Users on Company page  */
	@FindBy(how = How.XPATH, using="//tbody[@id='view_trp_alloc']/tr/td[1]")
	WebElement TRP_User_TradeName;

	/* Locator for Outward Tradename in  Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='outwardGstin_listbox']/li")
	WebElement TRP_tradename;
	
	/* Locator for Inward  Tradename in  Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='inwardGstin_listbox']/li")
	WebElement TRP_tradename_Inward;
	
	/* Locator for GSTr#b  Tradename in  Users on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='GSTINselection_listbox']/li")
	WebElement TRP_tradename_GSTR3B;
	
	
	/*****   Outward Xpaths       ******/

	/* Locator Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='/outward']/a")
	WebElement outwardsupply;

	/* Locator select tradename - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[1]/span/span/span[2]")
	WebElement outward_selecttradename;

	/* Locator select tradename list - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='outwardGstin_listbox']/li[1]")
	WebElement outward_tradename_list;

	/* Locator select month - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[2]/span/span/span/span")
	WebElement outward_month;

	/* Locator select month list - Outward */
	@FindBy(how = How.XPATH, using="//a[@class='k-link' and contains(text(),'Feb')]")
	WebElement outward_month_feb;

	/* Locator import button - Outward */
	@FindBy(how = How.XPATH, using="//a[@id='importdata' and @messagelog='Imported Outward Data']")
	WebElement outward_importfile;

	/* Locator import excel - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='btn-excel']")
	WebElement outward_importfile_excel;
	
	/* Locator import excel text default template - Outward Invoice*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadform']/div[1]/div/div[1]/div/span/span/span[1]")
	WebElement outward_excel_IN_text1;
	
	/* Locator import excel text filesize - Outward Invoice*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadform']/div[1]/div/div[3]/div/div[2]")
	WebElement outward_excel_IN_text2;
	
	/* Locator import excel text default template - Outward CDN*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformcdn']/div[1]/div/div[1]/div/span/span/span[1]")
	WebElement outward_excel_CDN_text1;
	
	/* Locator import excel text filesize - Outward CDN*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformcdn']/div[1]/div/div[3]/div/div[2]")
	WebElement outward_excel_CDN_text2;
	
	/* Locator import excel text default template - Outward TOA*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformtoa']/div[1]/div/div[1]/div/span/span/span[1]")
	WebElement outward_excel_TOA_text1;
	
	/* Locator import excel text filesize - Outward TOA*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformtoa']/div[1]/div/div[3]/div/div[2]")
	WebElement outward_excel_TOA_text2;
	
	/* Locator import excel text default template - Outward Doc Details*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformdoc']/div[1]/div/div[1]/div/span/span/span[1]")
	WebElement outward_excel_Doc_text1;
	
	/* Locator import excel text filesize - Outward Doc Details*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformdoc']/div[1]/div/div[3]/div/div[2]")
	WebElement outward_excel_Doc_text2;
	
	/* Locator import excel text default template - Outward HSN */
	@FindBy(how = How.XPATH, using=".//*[@id='uploadHSNform']/div[1]/div/div[1]/div/span/span/span[1]")
	WebElement outward_excel_hsn_text1;
	
	/* Locator import excel text filesize - Outward HSN */
	@FindBy(how = How.XPATH, using=".//*[@id='uploadHSNform']/div[1]/div/div[3]/div/div[2]")
	WebElement outward_excel_hsn_text2;
	
	/* Locator import excel text default template - Outward Invoice amendments */
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformInvAmend']/div[1]/div/div[1]/div/span/span/span[1]")
	WebElement outward_excel_INa_text1;
	
	/* Locator import excel text filesize - Outward Invoice amendments */
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformInvAmend']/div[1]/div/div[3]/div/div[2]")
	WebElement outward_excel_INa_text2;
	
	/* Locator import excel text default template - Outward CDN amendments */
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformcdnAmend']/div[1]/div/div[1]/div/span/span/span[1]")
	WebElement outward_excel_INc_text1;
	
	/* Locator import excel text filesize - Outward CDN amendments */
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformcdnAmend']/div[1]/div/div[3]/div/div[2]")
	WebElement outward_excel_INc_text2;
	
	
	
	/* Locator import csv - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='btn-csv']")
	WebElement outward_importfile_CSV;
	
	/* Locator import csv verify text - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='csv_uploadform']/div[1]/div/div/div/div[2]")
	WebElement outward_CSV_text1;
	
	/* Locator import rest api - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='importTypeModal']/div/div/div[2]/div[1]/button[contains(@class,'disabled') and contains(text(),'REST Api')]")
	WebElement outward_importfile_restapi;
	
	/* Locator import database - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='importTypeModal']/div/div/div[2]/div[2]/button[contains(@class,'disabled') and contains(text(),'Database')]")
	WebElement outward_importfile_database;
	
	/* Locator import invoice - Outward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Invoice Register')]")
	WebElement outward_importfile_invoice;
	
	/* Locator import cdn - Outward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Amendments Register')]")
	WebElement outward_importfile_cdn;
	
	/* Locator import toa - Outward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Tax on Advance Register')]")
	WebElement outward_importfile_toa;
	
	/* Locator import doc detail - Outward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Doc Details Register')]")
	WebElement outward_importfile_doc;

	/* Locator import invoice amd - Outward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Invoice Amend Register')]")
	WebElement outward_importfile_amedi;
	
	/* Locator import cdn amd - Outward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Credit/Debit Note Amend Register')]")
	WebElement outward_importfile_amedc;

	/* Locator close import excel  - Outward */
	//@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Invoice Register')]/parent::div/div/a/span[@class='k-icon k-i-close']")
	@FindBy(how = How.XPATH, using="html/body/div[8]/div[1]/div/a")
	WebElement outward_importfileexcel_Close;
	
	/* Locator close import csv - Outward */
	@FindBy(how = How.XPATH, using="html/body/div[9]/div[1]/div/a/span")
	WebElement outward_importfilecsv_Close;


	/* Locator save to gstr1 - Outward */
	@FindBy(how = How.XPATH, using=".//*[@id='submit']")
	WebElement outward_save;

	/* Locator verify save to gstr1 error popup message - Outward */
	@FindBy(how = How.XPATH, using="//div[@class='error_messages']/b")
	WebElement outward_save_errormessage;

	/* Locator close save to gstr1 error popup message - Outward */
	@FindBy(how = How.XPATH, using="html/body/div[18]/div[1]/div/a/span")
	WebElement outward_save_errorpopup;

	/* Locator Outward invoice tab */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardInvoice' and contains(text(),'Invoice')]")
	WebElement outward_invoice;

	/* Locator Outward CDN tab */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardAmendments' and contains(text(),'Credit / Debit Note')]")
	WebElement outward_cdn;

	/* Locator Outward TOA tab */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardTOA' and contains(text(),'Tax On Advance')]")
	WebElement outward_TOA;

	/* Locator Outward doc detail tab */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardDOC' and contains(text(),'Doc Detail')]")
	WebElement outward_doc;

	/* Locator Outward amendments dropdown */
	@FindBy(how = How.XPATH, using="//span[@data-toggle='dropdown' and contains(text(),'Amendments')]")
	WebElement outward_Amendment_dropdown;

	/* Locator Outward amendments - invoice tab */
	@FindBy(how = How.XPATH, using="//a[@id='invoicegstr1A'and contains(text(),'Invoice')]")
	WebElement outward_Amendment_invoice;

	/* Locator Outward amendments - CDN tab */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardAmendAMD' and contains(text(),'Credit / Debit Note')]")
	WebElement outward_Amendment_CDN;

	/* Locator Outward HSN summary tab */
	@FindBy(how = How.XPATH, using="//a[@grid-id='outwardHSN' and contains(text(),'HSN Summary')]")
	WebElement outward_HSN;
	
	
	

	/*
	 * Inward
	 */

	/* Locator Inward Supply */
	@FindBy(how = How.XPATH, using=".//*[@id='/inward']/a")
	WebElement inwardsupply;	

	/* Locator Inward Supply - Invoice click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[1]/a")
	WebElement inwardinvoice;

	/* Locator Inward Supply - CDN click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/a")
	WebElement inwardcdn;

	/* Locator Inward Supply - Tax Liability click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[3]/a")
	WebElement inwardtaxliability;

	/* Locator Inward Supply - ITC Reversal click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[4]/a")
	WebElement inwardITC_reversal;

	/* Locator Inward Supply - ISD Credit click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[5]/a")
	WebElement inwardISD;

	/* Locator Inward Supply - TDS/TCS/HSN click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[6]/a")
	WebElement inwardTdsTcs;

	/* Locator verify text Inward Supply - TDS/TCS/HSN click */
	@FindBy(how = How.XPATH, using=".//*[@id='pending']/h2")
	WebElement text_inwardTdsTcs;

	/* Locator Inward Supply - ITC Available DD click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[8]/span")
	WebElement inwardITC_available;

	/* Locator Inward Supply - ITC Available Invoice click */
	@FindBy(how = How.XPATH, using=".//*[@id='inInvoiceITCAvail']")
	WebElement inwardITC_invoice;

	/* Locator Inward Supply - ITC Available CDN click */
	@FindBy(how = How.XPATH, using=".//*[@id='inAmendITCAvail']")
	WebElement inwardITC_CDN;

	/* Locator Inward Supply - GSTR2A DD click */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[9]/span")
	WebElement inwardGSTR2A;

	/* Locator Inward Supply - GSTR2A Invoice click */
	@FindBy(how = How.XPATH, using=".//*[@id='invoicegstr2a']")
	WebElement inwardGSTR2A_invoice;

	/* Locator Inward Supply - GSTR2A CDN click */
	@FindBy(how = How.XPATH, using=".//*[@id='amendgstr2a']")
	WebElement inwardGSTR2A_CDN;

	/* Locator Inward Supply - GSTR2A summary click */
	@FindBy(how = How.XPATH, using=".//*[@id='summarygstr2a']")
	WebElement inwardGSTR2A_summary;

	/* Locator select tradename - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[1]/span/span/span[1]")
	WebElement inward_selecttradename;

	/* Locator select tradename list - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='inwardGstin_listbox']/li[1]")
	WebElement inward_tradename_list;

	/* Locator select month - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[2]/span/span/span/span")
	WebElement inward_month;

	/* Locator select month list - Inward */
	@FindBy(how = How.XPATH, using="//a[@class='k-link' and contains(text(),'Feb')]")
	WebElement inward_month_feb;

	/* Locator import button - Inward */
	@FindBy(how = How.XPATH, using="//a[@messagelog='Imported Inward Data']")
	WebElement inward_importfile;

	/* Locator import excel - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='btn-excel']")
	WebElement inward_importfile_excel;

	/* Locator import csv - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='importTypeModal']/div/div/div[1]/div[2]/button[contains(@class,'disabled') and contains(text(),'CSV File')]")
	WebElement inward_importfile_csv;

	/* Locator import restapi - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='importTypeModal']/div/div/div[2]/div[1]/button[contains(@class,'disabled') and contains(text(),'REST Api')]")
	WebElement inward_importfile_restapi;

	/* Locator import database - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='importTypeModal']/div/div/div[2]/div[2]/button[contains(@class,'disabled') and contains(text(),'Database')]")
	WebElement inward_importfile_database;

	/* Locator import invoice - Inward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Invoice Register')]")
	WebElement inward_importfile_invoice;

	/* Locator import cdn - Inward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Amendments Register')]")
	WebElement inward_importfile_cdn;

	/* Locator import tax liability - Inward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Tax Liability Register')]")
	WebElement inward_importfile_taxliability;

	/* Locator import ITC Reversal - Inward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload ITC Register')]")
	WebElement inward_importfile_ITC_Reversal;

	/* Locator import ISD Credit - Inward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload ISD Register')]")
	WebElement inward_importfile_ISD;

	/* Locator import ITC Available Invoice & CDN - Inward */
	@FindBy(how = How.XPATH, using="//span[@id='importTypeModal_wnd_title' and contains(text(),'Upload Available ITC Register')]")
	WebElement inward_importfile_ITC_available;

	/* Locator import excel text filesize - Inward Invoice*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadform']/div[1]/div/div/div/div[2]")
	WebElement inward_excel_IN_text1;

	/* Locator import excel text filesize - Inward CDN*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformcdn']/div[1]/div/div/div/div[2]")
	WebElement inward_excel_CDN_text1;

	/* Locator import excel text filesize - Inward Tax Liability*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformtoa']/div[1]/div/div/div/div[2]")
	WebElement inward_excel_tl_text1;

	/* Locator import excel text filesize - Inward ITC Reversal*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformitc']/div[1]/div/div/div/div[2]")
	WebElement inward_excel_itc_text1;

	/* Locator import excel text filesize - Inward ISD Credit*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformisd']/div[1]/div/div/div/div[2]")
	WebElement inward_excel_isd_text1;

	/* Locator import excel text filesize - Inward ITC Available Invoice*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformitcavial']/div[1]/div/div/div/div[2]")
	WebElement inward_excel_itci_text1;

	/* Locator import excel text filesize - Inward ITC Available CDN*/
	@FindBy(how = How.XPATH, using=".//*[@id='uploadformitcavial']/div[1]/div/div/div/div[2]")
	WebElement inward_excel_itcc_text1;

	/* Locator close import excel  - Inward invoice */
	@FindBy(how = How.XPATH, using="html/body/div[12]/div[1]/div/a/span")
	WebElement inward_importfile_excel_Close;

	/* Locator save to gstr2 - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='submitinword']")
	WebElement inward_save;

	/* Locator verify save to gstr2 error popup message - Inward */
	@FindBy(how = How.XPATH, using="//div[@class='error_messages']/b")
	WebElement inward_save_errormessage;

	/* Locator close save to gstr2 error popup message - Inward */
	@FindBy(how = How.XPATH, using="html/body/div[24]/div[1]/div/a/span")
	WebElement inward_save_errorpopupclose;

	/* Locator get GSTR2A - Inward */
	@FindBy(how = How.XPATH, using=".//*[@id='recieve']")
	WebElement inward_get_gstr2a;

	/* Locator enter otp - Inward get gstr2a */
	@FindBy(how = How.XPATH, using=".//*[@id='OTP']")
	WebElement inward_get_gstr2a_otp;

	/* Locator enter otp submit - Inward get gstr2a */
	@FindBy(how = How.XPATH, using=".//*[@id='validateForm']")
	WebElement inward_get_gstr2a_submit;

	/* Locator text for success modal - Inward get gstr2a */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div")
	WebElement inward_gstr2a_response_text;

	/* Locator close success modal - Inward get gstr2a */
	@FindBy(how = How.XPATH, using="html/body/div[24]/div[1]/div/a/span")
	WebElement inward_gstr2a_response_close;
	
	
	
	
	/*****  GSTR3B xpaths     ******/

	/* Locator GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='/gstr3b']/a")
	WebElement gstr3b;

	/* Locator select tradename - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[1]/span/span/span[1]")
	WebElement gstr3b_selecttradename;

	/* Locator select tradename list - GSTR3B */
	@FindBy(how = How.XPATH, using="//ul[@id='GSTINselection_listbox']/li[1]")
	WebElement gstr3b_tradename_list;

	/* Locator select month - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[1]/div/div[2]/span/span/span/span[2]/span")
	WebElement gstr3b_month;

	/* Locator select month list- GSTR3B */
	@FindBy(how = How.XPATH, using="//table[@class='k-content k-meta-view k-year']/tbody/tr[1]/td[1]")
	WebElement gstr3b_month_list;

	/* Locator Compiled tab - GSTR3B */
	@FindBy(how = How.XPATH, using="//a[@id='gstr3bComp' and contains(text(),'GSTR3B Compiled')]")
	WebElement gstr3b_compiled;

	/* Locator input tab - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='inputConf']")
	WebElement gstr3b_input;

	/* Locator process data button - GSTR3B */
	@FindBy(how = How.XPATH, using="//form[@id='FormNameStoreCompiled']/div[1]/a[contains(text(),'Process Data')]")
	WebElement gstr3b_processdata;

	/* Locator compile save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='compiled_to_enablegst']")
	WebElement cgstr3b_savetoenablegst;

	/* Locator response messgae after save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using="//div[@class='error_messages']")
	WebElement cgstr3b_saveresponse;

	/* Locator close response messgae after save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using="//div/span[@id='globalErrorModal_wnd_title']/parent::div/div/a")
	WebElement cgstr3b_saveresponseclose;

	/* Locator save to gstn button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='compiled_to_gstn']")
	WebElement cgstr3b_savetogstn;

	/* Locator save to gstn confirmation button - GSTR3B */
	@FindBy(how = How.XPATH, using="//button[@id='save_approve' and contains(text(),'Yes')]")
	WebElement cgstr3b_savetogstn_yes;

	/* Locator response window - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div")
	WebElement response;

	/* Locator save to gstn OTP button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='OTP']")
	WebElement cgstr3b_otp;

	/* Locator save to gstn OTP submit - GSTR3B */
	@FindBy(how = How.XPATH, using="//button[@id='validateForm' and contains(text(),'Submit')]")
	WebElement cgstr3b_otp_submit;

	/* Locator close response messgae after save to gstn - GSTR3B */
	@FindBy(how = How.XPATH, using="//div/span[@id='globalErrorModal_wnd_title']/parent::div/div/a")
	WebElement cgstr3b_gstnresponseclose;

	/* Locator verify response messgae after save to gstn - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div")
	WebElement cgstr3b_gstnresponsetextverify;


	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='add_to_enablegst']")
	WebElement igstr3b_savetoenablegst;

	
	
	

	/****** Returns *****/
	
	/* Locator Returns GSTR1 summary tab */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td[1]/span")
	List <WebElement> Returns_GSTR1_List;
	

	/* Locator Returns Send for Approval Enabled summary tab */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td[3]/span")
	List <WebElement>  Returns_SendEnable_List;
	
	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td/button [@id='GSTR1Pend']")
	WebElement Pending_Returns;
	
	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='allcontentpdf']/h3[contains(text(),'Form GSTR-1')]")
	WebElement Form_GSTR1;
	
	
	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='approve']")
	WebElement Accept_Form_GSTR1;
	

	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='reject']")
	WebElement Reject_Form_GSTR1;
	

	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using=".//*[@id='GSTR1StatusD']")
	WebElement Reject_Status_Form_GSTR1;
	
	
	/* Locator Returns Send for Approval Enabled summary tab */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td[3]/button[contains(text(),'SEND') and contains(@class,'disabled')]")
	List <WebElement>  SendForApp_Disable;
	
	/* Locator Returns Send for Approval Enabled summary tab */
	@FindBy(how = How.XPATH, using=".//*[@id='returns']/tbody/tr/td[4]/button[contains(@class,'disabled')]")
	List <WebElement>  BlankApproval;
	

	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using="//div[@id='datePicker_dateview']/div/div[@class='k-header']/a[2]")
	WebElement Calender_Prev_Year;
	
	
	/* Locator input save to enablegst button - GSTR3B */
	@FindBy(how = How.XPATH, using="//div[@id='datePicker_dateview']/div/table/tbody/tr[3]/td[1]/a[contains(text(),'Sep')]")
	WebElement Sep_Month_Returns;
	
	
	/********  Utilities Xpaths   *********/

	/* Locator for home */
	@FindBy(how = How.XPATH, using=".//*[@id='/home']/a")
	WebElement home;

	/* Locator for utilities */
	@FindBy(how = How.XPATH, using=".//*[@id='navbarNav']/ul/li[8]/span")
	WebElement utilities;

	/* Locator for utilities */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a")
	List <WebElement> utilities_list;

	/* Locator for customer master */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Customer Master')]")
	WebElement customermaster;

	/* Locator for vendor master */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Vendor Master')]")
	WebElement vendormaster;

	/* Locator for HSN item master */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'HSN Item Master')]")
	WebElement hsn_item_master;

	/* Locator for invoice print */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Invoice Print')]")
	WebElement invoiceprint;

	/* Locator for invoice template */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Invoice Template')]")
	WebElement invoicetemplate;

	/* Locator for Past Returns */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Past Returns')]")
	WebElement pastreturns;

	/* Locator for remove records */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Remove Records')]")
	WebElement removerecords;

	/* Locator for search HSN */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Search HSN')]")
	WebElement searchhsn;

	/* Locator for labels */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Labels')]")
	WebElement labels;

	/* Locator for delete saved records */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Delete Saved Records')]")
	WebElement delete_saved_records;

	/* Locator for custom template mapping */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Custom Template Mapping')]")
	WebElement customertemplatemapping;

	/* Locator for Bulk upload */
	@FindBy(how = How.XPATH, using="//ul[@class='navbar-nav border-bot float-left']/li/div/a[contains(text(),'Bulk Upload')]")
	WebElement bulkupload;

	/* Locator for update from transaction - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='updateFromTx']")
	WebElement cm_updatefromtransaction;

	/* Locator for update from transaction - customer master */
	@FindBy(how = How.XPATH, using="//span[@id='globalErrorModal_wnd_title']/parent::div/div/a/span")
	WebElement cm_updatefromtransaction_close;

	/* Locator for verify text after update from transaction - customer master */
	@FindBy(how = How.XPATH, using="//div[@class='error_messages']")
	WebElement cm_updatefromtransaction_verify;

	/* Locator for get from gstn - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='getFromGSTN']")
	WebElement cm_getfromgstn;

	/* Locator for close response get from gstn - customer master */
	@FindBy(how = How.XPATH, using="//span[@id='globalErrorModal_wnd_title']/parent::div/div/a/span")
	WebElement cm_getfromgstn_close;

	/* Locator for verify response of get from gstn - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div")
	WebElement cm_getfromgstn_verify;

	/* Locator for invites - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/span")
	WebElement cm_invites;

	/* Locator for invites list - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/div/a")
	WebElement cm_inviteslist;

	/* Locator for invites sent - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/div/a[1]")
	WebElement cm_sent;

	/* Locator for invites received - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/div/a[2]")
	WebElement cm_received;

	/* Locator for update from transaction - vendor master */
	@FindBy(how = How.XPATH, using=".//*[@id='updateFromTx']")
	WebElement vm_updatefromtransaction;

	/* Locator for get from gstn - vendor master */
	@FindBy(how = How.XPATH, using=".//*[@id='getFromGSTN']")
	WebElement vm_getfromgstn;

	/* Locator for invites - vendor master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/span")
	WebElement vm_invites;

	/* Locator for invites list - vendor master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/div/a")
	WebElement vm_inviteslist;

	/* Locator for invites sent - vendor master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/div/a[1]")
	WebElement vm_sent;

	/* Locator for invites received - vendor master */
	@FindBy(how = How.XPATH, using=".//*[@id='style-2']/li[2]/div/a[2]")
	WebElement vm_received;

	/*
	 * HSN item master
	 */

	/* Locator for update from masters - HSN item master */
	@FindBy(how = How.XPATH, using=".//*[@id='fetch']")
	WebElement updatefrommasters;

	/* Locator for verify response of get from gstn - customer master */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div")
	WebElement hsnitemmaster_verify;

	/* Locator for close response get from gstn - customer master */
	@FindBy(how = How.XPATH, using="//span[@id='globalErrorModal_wnd_title']/parent::div/div/a/span")
	WebElement hsnitemmaster_close;


	/*
	 * Invoice template
	 */

	/* Locator for add template - Invoice template */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[2]/div[1]/div/div/div/div/div/div[3]/button")
	WebElement add_template;

	/* Locator for close add template - Invoice template */
	@FindBy(how = How.XPATH, using="//span[@id='addTemplate_wnd_title']/parent::div/div/a/span")
	WebElement addtemplate_close;

	/*
	 * Past returns
	 */

	/* Locator for select tradename- past returns */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[2]/div[1]/div/div/div/div/div/div/div/div[1]/span/span/span[1]")
	WebElement selecttradename_pastreturns;

	/* Locator for select 1st tradename- past returns */
	@FindBy(how = How.XPATH, using=".//*[@id='getReturnsGstin_listbox']/li[2]")
	WebElement tradename_pastreturns;

	/* Locator select month - past returns */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[2]/div[1]/div/div/div/div/div/div/div/div[2]/div/div[1]/span/span/span/span[2]")
	WebElement ps_month;

	/* Locator select month list- past returns */
	@FindBy(how = How.XPATH, using="//table[@class='k-content k-meta-view k-year']/tbody/tr[1]/td[1]")
	WebElement ps_month_list;

	/* Locator for get status - past returns */
	@FindBy(how = How.XPATH, using=".//*[@id='getRetTrack']")
	WebElement getstatus;

	/* Locator close response get status - past return */
	@FindBy(how = How.XPATH, using="//div/span[@id='globalErrorModal_wnd_title']/parent::div/div/a")
	WebElement past_responseclose;

	/* Locator response window - past return */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div")
	WebElement pastreturn_response;


	/* Locator save to gstn OTP button - past return */
	@FindBy(how = How.XPATH, using=".//*[@id='OTP1']")
	WebElement pastreturn_otp;

	/* Locator save to gstn OTP submit - past return */
	@FindBy(how = How.XPATH, using=".//*[@id='validateForm1']")
	WebElement pastreturn_otp_submit;

	/* Locator close response messgae after save to gstn - past return */
	@FindBy(how = How.XPATH, using="//div/span[@id='globalErrorModal_wnd_title']/parent::div/div/a")
	WebElement pastreturn_gstnresponseclose;

	/* Locator verify response messgae after save to gstn - past return */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div")
	WebElement pastreturn_gstnresponsetextverify;

	/*
	 * Labels
	 */

	/* Locator for Add New button - labels */
	@FindBy(how = How.XPATH, using=".//*[@id='labelsGrid']/div[1]/a")
	WebElement addnewlabel;

	/* Locator for Close Add New modal - labels */
	@FindBy(how = How.XPATH, using="html/body/div[12]/div[1]/div/a/span")
	WebElement Close_labelmodal;

	/*
	 * Custom template
	 */

	/* Locator for select tradename dropdwon - custom template upload */
	@FindBy(how = How.XPATH, using=".//*[@id='marginlessbuttons']/div[2]/div[1]/div/div/div/div[1]/span/span/span[1]")
	WebElement selecttradename_dd;

	/* Locator for select tradename - custom template upload */
	@FindBy(how = How.XPATH, using=".//*[@id='gstinDD_listbox']/li[1]")
	WebElement selecttradename;

	/* Locator for view template - custom template upload */
	@FindBy(how = How.XPATH, using=".//*[@id='existingTemplates']")
	WebElement viewtemplate;

	/* Locator for create template - custom template upload */
	@FindBy(how = How.XPATH, using=".//*[@id='createTemplate']")
	WebElement createtemplate;

	/* Locator for close create template modal - custom template upload */
	@FindBy(how = How.XPATH, using="html/body/div[6]/div[1]/div/a")
	WebElement close_createtemplate;
	
	
	//Location Add - Turnover 2016-2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2016-2017')]/parent::div/div/input[@id='addLoc_avg_turnover']")
	WebElement loc_addturnover1;

	//Location Add - Turnover April-June 2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover April-June 2017')]/parent::div/div/input[@id='addLoc_curr_turnover']")
	WebElement loc_addturnover2;

	//Location Add - Turnover 2017-2018
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2017-2018')]/parent::div/div/input[@id='addLoc_avg_turnover1']")
	WebElement loc_addturnover3;

	//Location Edit - Turnover 2016-2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2016-2017')]/parent::div/div/input[@id='editLoc_avg_turnover']")
	WebElement loc_editturnover1;

	//Location Edit - Turnover April-June 2017
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover April-June 2017')]/parent::div/div/input[@id='editLoc_curr_turnover']")
	WebElement loc_editturnover2;

	//Location Edit - Turnover 2017-2018
	@FindBy(how = How.XPATH,using="//p[contains(text(),'Turnover 2017-2018')]/parent::div/div/input[@id='editLoc_avg_turnover1']")
	WebElement loc_editturnover3;

	//Location View - Turnover 2016-2017
	@FindBy(how = How.XPATH,using=".//*[@id='viewLoc_avg_turnover']")
	WebElement loc_viewturnover1;

	//Location View - Turnover April-June 2017
	@FindBy(how = How.XPATH,using=".//*[@id='viewLoc_curr_turnover']")
	WebElement loc_viewturnover2;

	//Location View - Turnover 2017-2018
	@FindBy(how = How.XPATH,using=".//*[@id='viewLoc_avg_turnover1']")
	WebElement loc_viewturnover3;
	
	
	
	
	
	
	
	
	
	
	
	
}

