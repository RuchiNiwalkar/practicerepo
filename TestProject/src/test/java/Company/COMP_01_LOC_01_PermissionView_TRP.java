package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_PermissionView_TRP extends CompanyMethods
{
	@Test
	public void ViewRole_Client() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Verify Group & Email-ID
		CM.ViewGroup_Email_Id_Num();

		//Select Users
		CM.SelectUsers();

		//Selects the TradeName from the specified TRP user
		CM.TRP_TradeNamePermission("trpviewrole1@mailinator.com");

		//Login into Test
		CM.Login_TRP("trpviewrole1@mailinator.com");

		//Select Group
		CM.SelectTRP();

		//Verify Outward Tab
		CM.TRP_Outward();

		//Verify Inward Tab
		CM.TRP_Inward();

		//Verify GSTR3B
		CM.GSTR3BView_TRP();

		//Returns
		CM.Returns();

		//Verify Returns
		CM.VerifyReturns();

		//Verify Utilities
		CM.Utilities();

		softassert.assertAll();

	}

}
