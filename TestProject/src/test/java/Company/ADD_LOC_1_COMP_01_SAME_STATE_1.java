package Company;

import org.testng.annotations.Test;

public class ADD_LOC_1_COMP_01_SAME_STATE_1 extends CompanyMethods 
{
	@Test
	public void COMP_01_SAME_STATE_1() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Location
		CM.AddLocation_SameState_01();

		//Add Address Company
		CM.AddAddressLoc();

		//Add GSTN_Username Company
		CM.addGSTNLoc();

		//Verify the company in the company List
		CM.HSN_ITC_Loc();

		//Verify the Location in the company List	
		CM.VerifyLoc01();

		softassert.assertAll();
	}
	
}
