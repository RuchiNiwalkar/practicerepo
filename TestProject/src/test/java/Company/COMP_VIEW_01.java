package Company;

import org.testng.annotations.Test;

public class COMP_VIEW_01 extends CompanyMethods {

	@Test
	public void VIEW_01() throws Exception
	{		
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//View the Company Added
		CM.ViewCompany();

		//View the OtherConfig Selected
		CM.ViewOtherConfigDeselected();

		softassert.assertAll();

	}
}


