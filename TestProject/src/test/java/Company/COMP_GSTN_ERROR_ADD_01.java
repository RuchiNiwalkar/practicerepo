package Company;

import org.testng.annotations.Test;

public class COMP_GSTN_ERROR_ADD_01 extends CompanyMethods
{
	@Test
	public void COMP_GSTN_ERROR_01() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Company
		CM.AddCompany();

		//Add Address Company
		CM.AddAddress();

		//Add GSTN_Username Company
		CM.addGSTN();

		//Verify the company in the company List
		CM.HSN_ITC();

		//Verify the company in the company List	
		CM.VerifyGSTNError_Company();

		softassert.assertAll();

	}

}
