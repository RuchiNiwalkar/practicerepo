package Company;

import org.testng.annotations.Test;

public class COMP_ADD_NEG_MANDATORY_02 extends CompanyMethods
{
	
	@Test
	public void COMP_ADD_NEG() throws Exception
	{
			
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Company
		CM.Comp_NavigateOtherConfig_Neg02();
		
		//CLick on Save
		CM.SaveComp_Neg();

		//Check first page error
		CM.Check_Mandatory_Errors01();
		
		//Check second page error
		CM.Check_Mandatory_Errors_02();
		
		//Check third page error
		CM.Check_Mandatory_Errors_03();
		
		//Check fourth page error
		CM.Check_Mandatory_Errors_04();
		
		softassert.assertAll();
	}

}
