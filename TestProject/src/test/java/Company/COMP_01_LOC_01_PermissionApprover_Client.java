package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_PermissionApprover_Client extends CompanyMethods
{	
	@Test
	public void OutwardRole_Client() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Users
		CM.SelectUsers();

		//Selects the TradeName from the specified clientadmin user
		CM.TradeNamePermission("groupapproverusers16@mailinator.com");

		//Login into Test
		CM.Login_Permission("groupapproverusers16@mailinator.com");

		/*//Verify Outward Tab
		CM.Outward();

		//Verify Inward Tab
		CM.Inward();

		//Verify GSTR3B
		CM.GSTR3BView();
*/
		CM.Returns();
		
		//Returns
		CM.ReturnsEnabled();

	/*	//Verify Returns
		CM.VerifyReturns();

		//Verify Utilities
		CM.Utilities();*/

		softassert.assertAll();
	}
}
