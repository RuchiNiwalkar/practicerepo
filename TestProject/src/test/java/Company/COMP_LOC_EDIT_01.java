package Company;

import org.testng.annotations.Test;

public class COMP_LOC_EDIT_01 extends CompanyMethods
{
	@Test
	public void LOC_VIEW() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//View the Location Added
		CM.EditCompany_Location_01();
		
		
		softassert.assertAll();

	}

}
