package Company;

import org.testng.annotations.Test;

public class COMP_EDIT_02 extends CompanyMethods 

{

	@Test
	public void EDIT_02() throws Exception	
	{
		CompanyMethods CM = new CompanyMethods();

		//Login
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Edit the Company Added
		CM.EditCompany02();

		//Verify edited company in List
		CM.VerifyCompanyEdited02();

		softassert.assertAll();
	}

}
