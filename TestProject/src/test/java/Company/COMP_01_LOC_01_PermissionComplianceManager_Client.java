package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_PermissionComplianceManager_Client extends CompanyMethods
{
	@Test
	public void PermissionComplianceManager_Client() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		CM.Login_Roles("groupcmusers16@mailinator.com");

		CM.SelectCompany();

		CM.VerifyforRoles();

		//Login into Test
		CM.Login();

		//Select Users
		CM.SelectUsers();

		//Selects the TradeName from the specified clientadmin user
		CM.TradeNamePermission("groupcmusers16@mailinator.com");

		//Login into Test
		CM.Login_Permission("groupcmusers16@mailinator.com");

		//Verify Outward Tab
		CM.OutwardClient_Enabled();

		//Verify Inward Tab
		CM.InwardClient_Enable();
				
		//Verify GSTR3B
		CM.GSTR3B_Enabled();
		
		//Verify Returns 
		CM.ReturnsEnabled();
		

		//Utilities
		CM.utilities_CM();
		
		softassert.assertAll();

	}



}
