package Company;

import org.testng.annotations.Test;

public class LOC_ADD_NEG_01 extends CompanyMethods
{
	@Test
	public void ADD_NEG_01() throws Exception 
	{

		CompanyMethods CM = new CompanyMethods();
		
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Location
		CM.AddLocation01_Neg();

		//Add Address Company
		CM.AddAddressLoc01_Neg();

		//Add GSTN_Username Company
		CM.addGSTNLoc01_Neg();

		//Verify the company in the company List
		CM.HSN_ITC_Loc01_Neg();

		//verify save Location
		CM.Save_Loc01_Neg();

		//Check first page error
		CM.Check_LOC_01_Errors_01();
		
		//Check second page error
		CM.Check_LOC_01_Errors_02();
		
		//Check third page error
		CM.Check_LOC_01_Errors_03();
		
		//Check fourth page error
		CM.Check_LOC_01_Errors_04();

		softassert.assertAll();

	}

}
