package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_RoleOutward_TRP extends CompanyMethods
{
	@Test
	public void RoleOutward_TRP() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		CM.Login_Roles("groupoutwardusers16@mailinator.com");

		CM.SelectCompany();

		CM.VerifyforRoles();

		softassert.assertAll();
	}

}
