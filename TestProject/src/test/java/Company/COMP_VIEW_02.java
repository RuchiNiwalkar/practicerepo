package Company;

import org.testng.annotations.Test;

public class COMP_VIEW_02 extends CompanyMethods
{

	@Test
	public void VIEW_02() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//View the Company Added
		CM.ViewCompany02();

		//View the OtherConfig Selected
		CM.ViewOtherConfigDeselected02();

		softassert.assertAll();

	}

}
