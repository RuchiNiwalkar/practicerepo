package Company;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class COMP_LOC_ADD_01  extends CompanyMethods{

	@Test
	public void LOC_01() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Location
		CM.AddLocation();

		//Add Address Company
		CM.AddAddressLoc();

		//Add GSTN_Username Company
		CM.addGSTNLoc();

		//Verify the company in the company List
		CM.HSN_ITC_Loc();

		//Verify the Location in the company List	
		CM.VerifyLoc01();

		softassert.assertAll();

	}

}
