package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_RoleInward_TRP extends CompanyMethods 
{
	@Test
	public void RoleInward_TRP() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		CM.Login_Roles("trpinwardrole1@mailinator.com");

		CM.SelectCompany();

		CM.VerifyforRoles();

		softassert.assertAll();
	}

}