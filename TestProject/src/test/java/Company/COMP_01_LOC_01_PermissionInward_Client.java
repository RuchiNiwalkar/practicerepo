package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_PermissionInward_Client extends CompanyMethods
{
	@Test
	public void OutwardRole_Client() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Users
		
		CM.SelectUsers();

		//Selects the TradeName from the specified clientadmin user
		CM.TradeNamePermission("groupinwardusers16@mailinator.com");

		//Login into Test
		CM.Login_Permission("groupinwardusers16@mailinator.com");

		//Verify Outward Tab
		CM.Outward();

		//Verify Inward Tab
		CM.InwardClient_Enable();

		//Verify GSTR3B
		CM.GSTR3BView();

		//Returns
		CM.Returns();

		//Verify Returns
		CM.VerifyReturns();

		//Verify Utilities
		CM.Utilities();

		softassert.assertAll();

	}

}
