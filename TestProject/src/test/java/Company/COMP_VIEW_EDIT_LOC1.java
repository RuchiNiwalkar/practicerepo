package Company;

import org.testng.annotations.Test;

public class COMP_VIEW_EDIT_LOC1 extends CompanyMethods {


	@Test
	public void LOC_EDIT_VIEW() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//View Location
		CM.Edit_ViewLocation();

		//Verify the company in the company List
		CM.Location_1_Edit_ViewOtherConfigDeselected();
		
		softassert.assertAll();

	}

}
