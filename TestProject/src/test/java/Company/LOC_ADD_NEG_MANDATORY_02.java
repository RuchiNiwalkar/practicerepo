package Company;

import org.testng.annotations.Test;

public class LOC_ADD_NEG_MANDATORY_02 extends CompanyMethods
{

	@Test
	public void ADD_NEG_01() throws Exception 
	{

		CompanyMethods CM = new CompanyMethods();
		
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();


		//Navigtae to Other Configuration tab
		CM.Loc_NavigateOtherConfig_Neg02();

		//Click on save Location
		CM.Save_Loc01_Neg();

		//Check first page error
		CM.Mandatory_Check_LOC_01_Errors_01();
		 
		//Check second page error
		CM.Mandatory_Check_LOC_01_Errors_02();
		
		//Check third page error
		CM.Mandatory_Check_LOC_01_Errors_03();
		
		//Check fourth page error
		CM.Mandatory_Check_LOC_01_Errors_04();

		softassert.assertAll();
		

	}
	
}
