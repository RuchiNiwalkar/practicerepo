package Company;

import org.testng.annotations.Test;

public class COMP_LOC_VIEW_02 extends CompanyMethods
{

	@Test
	public void LOC_VIEW_02() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//View Location
		CM.ViewLocation02();
		
		//Verify the company in the company List
		CM.Location_1_ViewOtherConfigDeselected();

		softassert.assertAll();

	}

}

