package Company;

import org.testng.annotations.Test;

public class COMP_EDIT_01 extends CompanyMethods{

	@Test
	public void EDIT_01() throws Exception	
	{
		CompanyMethods CM = new CompanyMethods();

		//Login
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Edit the Company Added
		CM.EditCompany();

		//Verify edited company in List
		CM.VerifyCompanyEdited();

		softassert.assertAll();
	}

}
