package Company;

import org.testng.annotations.Test;

public class LOC_01_GSTN_ERROR_01 extends CompanyMethods
{
	@Test
	public void LOC_01_GSTN_ERROR() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Location
		CM.AddLocation();

		//Add Address Company
		CM.AddAddressLoc();

		//Add GSTN_Username Company
		CM.addGSTNLoc();

		//Verify the company in the company List
		CM.HSN_ITC_Loc01_GSTINError();

		//Verify the error msg of duplicate GSTIN
		CM.VerifyGSTNError_Location_01();
		
		softassert.assertAll();





	}
}
