package Company;

import org.testng.annotations.Test;

public class LOC_PAN_ERROR_01 extends CompanyMethods
{
	@Test
	public void COMP_PAN_ERROR_COMP() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Location
		CM.AddLocation_PanError();

		//Add Address Company
		CM.AddAddressLoc_PanError();

		//Add GSTN_Username Company
		CM.addGSTNLoc_PanError();

		//Verify the company in the company List
		CM.HSN_ITC_Loc_PanError();
		
		//Verify mismatch output
		CM.VerifyPANError_Location_01();

		softassert.assertAll();

	}	

}
