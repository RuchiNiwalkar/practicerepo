package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_PermissionOutward_Client extends CompanyMethods
{
	@Test
	public void OutwardRole_Client() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Users
		CM.SelectUsers();

		//Selects the TradeName from the specified clientadmin user
		CM.TradeNamePermission("groupoutwardusers16@mailinator.com");

		//Login into Test
		CM.Login_Permission("groupoutwardusers16@mailinator.com");

		//Verify Outward Tab
		CM.OutwardClient_Enabled();

		//Verify Inward Tab
		CM.Inward();

		//Verify GSTR3B
		CM.GSTR3BView();

		//Returns
		CM.Returns();

		//Verify Returns
		CM.VerifyReturns();

		//Verify Utilities
		CM.Utilities();

		softassert.assertAll();

	}

}
