package Company;

import org.testng.annotations.Test;

public class COMP_VIEW_EDIT_02 extends CompanyMethods {

	@Test
	public void COMP_VIEW_EDIT() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//View the Company Edited
		CM.ViewEditedCompany02();

		//View the OtherConfig Selected
		CM.ViewEditOtherConfigDeselected02();		

	}


}
