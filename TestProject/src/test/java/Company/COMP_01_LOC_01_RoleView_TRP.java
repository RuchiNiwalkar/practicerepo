package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_RoleView_TRP extends CompanyMethods
{
	@Test
	public void ViewRole_TRP() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		
		//Login into Test
		CM.Login_Roles("trpviewrole1@mailinator.com");

		//Select Company
		CM.SelectCompany();
		
		//Verify for the role of View
		CM.VerifyforRoles();
		
		softassert.assertAll();
	}

}
