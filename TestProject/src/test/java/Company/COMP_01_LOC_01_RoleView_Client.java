package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_RoleView_Client extends CompanyMethods
{
	@Test
	public void ViewRole_Client() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		
		//Login into Test
		CM.Login_Roles("groupviewusers16@mailinator.com");

		//Select Company
		CM.SelectCompany();
		
		//Verify for the role of View
		CM.VerifyforRoles();
		
		softassert.assertAll();
	}

}
