package Company;

import org.testng.annotations.Test;

public class LOC_01_GSTN_STATECODE_MISMATCH_01 extends CompanyMethods
{
	@Test
	public void LOC_01_GSTN_STATECODE_MISMATCH() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Location
		CM.AddLocation_Mismatch();

		//Add Address Company
		CM.AddAddressLoc_Mismatch();

		//Add GSTN_Username Company
		CM.addGSTNLoc_Mismatch();

		//Verify the company in the company List
		CM.HSN_ITC_Loc_Mismatch();

		//Verify mismatch output
		CM.VerifyMismatch_Location_01();

		softassert.assertAll();
	}	

}
