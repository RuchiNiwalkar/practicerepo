package Company;

import org.testng.annotations.Test;

public class COMP_ADD_02 extends CompanyMethods {

	@Test
	public void CreateCompany02() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Company
		CM.AddCompany02();

		//Add Address Company
		CM.AddAddress02();
		
		//Add GSTN_Username Company
		CM.addGSTN02();
		
		//Verify the company in the company List
		CM.HSN_ITC02();

		//Verify the company in the company List	
		CM.VerifyCompany02();
 
		softassert.assertAll();
		
	}

}
