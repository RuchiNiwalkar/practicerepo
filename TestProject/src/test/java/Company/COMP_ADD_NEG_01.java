package Company;

import org.testng.annotations.Test;

public class COMP_ADD_NEG_01 extends CompanyMethods
{	
	@Test
	public void ADD_NEG_01() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Company
		CM.AddCompany01_Neg();

		//Add Address
		CM.AddAddress01_Neg();

		//Add GSTN
		CM.addGSTN01_Neg();

		//Add ITC or HSN
		CM.HSN_ITC01_Neg();

		//verify save company
		CM.SaveComp_Neg();

		//Check first page error
		CM.Check_Errors_01();
		
		//Check second page error
		CM.Check_Errors_02();
		
		//Check third page error
		CM.Check_Errors_03();
		
		//Check fourth page error
		CM.Check_Errors_04();
		
		softassert.assertAll();
	}

}
