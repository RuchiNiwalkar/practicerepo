package Company;

import org.testng.annotations.Test;

public class COMP_LOC_ADD_02 extends CompanyMethods 
{
	@Test
	public void LOC_02() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Location
		CM.AddLocation02();

		//Add Address Company
		CM.AddAddressLoc02();

		//Add GSTN_Username Company
		CM.addGSTNLoc02();

		//Verify the company in the company List
		CM.HSN_ITC_Loc();

		softassert.assertAll();

	}

}
