package Company;

import org.testng.annotations.Test;

public class COMP_GSTN_STATECODE_MISMATCH_01 extends CompanyMethods
{
	@Test
	public void COMP_GSTN_STATECODE_MISMATCH() throws Exception
	{
		
		CompanyMethods CM = new CompanyMethods();
		
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//Create Company
		CM.AddCompany_MisMatch();

		//Add Address Company
		CM.AddAddress_MisMatch();

		//Add GSTN_Username Company
		CM.addGSTN_MisMatch();

		//Verify the company in the company List
		CM.HSN_ITC_MisMatch();

		//Verify the company in the company List	
		CM.VerifyMisMatch_Company();

		softassert.assertAll();
	}	

}
