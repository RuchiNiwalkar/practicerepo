package Company;

import org.testng.annotations.Test;

public class COMP_01_LOC_01_RoleInward_Client extends CompanyMethods 
{
	@Test
	public void RoleInward_Client() throws Exception
	{

		CompanyMethods CM = new CompanyMethods();

		CM.Login_Roles("groupinwardusers16@mailinator.com");

		CM.SelectCompany();

		CM.VerifyforRoles();

		softassert.assertAll();
	}

}
