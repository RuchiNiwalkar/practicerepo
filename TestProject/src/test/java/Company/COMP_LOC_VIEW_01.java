package Company;

import org.testng.annotations.Test;

public class COMP_LOC_VIEW_01  extends CompanyMethods
{

	@Test
	public void LOC_VIEW_01() throws Exception
	{
		CompanyMethods CM = new CompanyMethods();
		//Login into Test
		CM.Login();

		//Select Company
		CM.SelectCompany();

		//View Location
		CM.ViewLocation();
		
		//Verify the company in the company List
		CM.Location_1_ViewOtherConfigDeselected();

		softassert.assertAll();

	}



}
