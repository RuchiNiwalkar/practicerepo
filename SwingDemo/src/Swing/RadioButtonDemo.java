package Swing;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class RadioButtonDemo
{
	public static void main(String[] args)
	{
		RadioDemo rd = new RadioDemo();
	}




}

class RadioDemo extends JFrame
{
	Scanner	 s = new Scanner(System.in);
	JLabel l;
	JRadioButton r1, r2;
	JTextField t1;
	JButton b;

	public RadioDemo()
	{
		l = new JLabel("Greeting");
		r1 = new JRadioButton("Male");
		r2 = new JRadioButton("Female");
		b = new JButton("OK");
		t1 = new JTextField(10);

		add(t1);
		add(r1);
		add(r2);
		add(b);
		add(l);

		ButtonGroup bg = new ButtonGroup();
		bg.add(r1);
		bg.add(r2);

		b.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String name = t1.getText();

				if(r1.isSelected())
				{
					name = " Mr.  "+ name;
				}
				else
				{
					name=" Miss.  "+name;
				}
				l.setText(name);

			}
		});
		setLayout(new FlowLayout());
		setVisible(true);
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}


}