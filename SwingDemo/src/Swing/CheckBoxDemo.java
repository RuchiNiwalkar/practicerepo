package Swing;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class CheckBoxDemo 
{
	
	public static void main(String[] args) 
	{
		
		Sample s = new Sample();
		
	}

}

class Sample extends JFrame
{
	JLabel l;
	JRadioButton r1, r2;
	JTextField t1;
	JButton b;
	JCheckBox c1 ;
	JCheckBox c2;
	
	
	public Sample()
	{
		l = new JLabel("Greeting");
		c1 = new JCheckBox("Swimming");
		c2 = new JCheckBox("Singer");
		r1 = new JRadioButton("Male");
		r2 = new JRadioButton("Female");
		b = new JButton("OK");
		t1 = new JTextField(10);
	

		add(t1);
		add(r1);
		add(r2);
		add(b);
		add(c1);
		add(c2);
		add(l);

		ButtonGroup bg = new ButtonGroup();
		bg.add(r1);
		bg.add(r2);

		b.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String name = t1.getText();

				if(r1.isSelected())
				{
					name = " Mr.  "+ name;
				}
				else
				{
					name=" Miss.  "+name;
				}
				if(c1.isSelected())
				{
					name = name + "  Dancer  ";
				}
				if(c2.isSelected())
				{
					name = name + "  Swimmer  ";
				}
				l.setText(name);

			}
		});
		setLayout(new FlowLayout());
		setVisible(true);
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
}