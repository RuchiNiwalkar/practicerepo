package Day4;

import java.util.InputMismatchException;
import java.util.Scanner;

import javax.rmi.ssl.SslRMIClientSocketFactory;

public class ArrayTest {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		ArrayNumber a = new ArrayNumber();
		a.setElements();
		a.printElements();
		a.calculateSum();
		a.setSum();
		
		try
		{
			System.out.println("Enter totaT sum & Total Number :");
			int AVG  = a.printAverage(s.nextInt(), s.nextInt()) ;
			System.out.println("AVG IS : "+AVG);
		}
		
		catch(ArithmeticException e )
		{
			System.out.println("Cannot divide by zero. Please enter any other number than zero ");
		}
			
		catch(IndexOutOfBoundsException e )
		{
			System.out.println("Array size is 7 , Cannot enter element further  ");
		}
		catch(InputMismatchException e )
		{
			System.out.println("Cannot accept String Datatype, Please enter an integer. ");
		}
		catch(Exception e )
		{
			System.out.println("Error, correct the program ");
		}
	}

}
