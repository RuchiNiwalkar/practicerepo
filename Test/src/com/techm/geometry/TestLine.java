package com.techm.geometry;

import java.util.Scanner;

import geometry.shapes.Line;
import geometry.shapes.Point;

public class TestLine {

	public static void main(String[] args)
	{
		
		Scanner s = new Scanner(System.in);
		Line l = new Line();
		Point p1 = new Point();
		System.out.println("Enter x & y coordinates for point p1 ");
		p1.setX(s.nextInt());
		p1.setY(s.nextInt());
		l.setPoint1(p1);
		System.out.println("Point p1 coordinates are : "+l.getPoint1().getX()+ " and "+l.getPoint1().getY());
		l.draw();
		l.scale();
		
		
		Point p2 = new Point();
		System.out.println("Enter x & y coordinates for point p2 ");
		p2.setX(s.nextInt());
		p2.setY(s.nextInt());
		l.setPoint2(p2);
		System.out.println("Point p2 coordinates are : "+l.getPoint2().getX()+ " and "+l.getPoint2().getY());
		l.draw();
		l.scale();
		
		
		
		

	}

}
