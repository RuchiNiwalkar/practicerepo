package Day2_Assignments;

public class BookData1  {
	Book[] books_comp = new Book[2];
	private int index;
	public void setBook(int bookNo, String title, String publication, String author, float price) 
	{
		for(index=0 ; index< books_comp.length ;index++)
		{
			books_comp[index] = new Computer();
			//System.out.println("Enter book No : ");
			books_comp[index].setBookNo(bookNo);
			books_comp[index].setTitle(title);
			books_comp[index].setPublication(publication);
			books_comp[index].setAuthor(author);
			books_comp[index].setPrice(price);
		}
	}
	public void printBooks()
	{
		for(index=0 ; index< books_comp.length ;index++)
		{
			System.out.println();
			System.out.println("Book details : ");
			System.out.println("Book no : "+books_comp[index].getBookNo());
			System.out.println("Book Title : "+books_comp[index].getTitle());
			System.out.println("Book Publication : "+books_comp[index].getPublication());
			System.out.println("Book Author : "+books_comp[index].getAuthor());
			System.out.println("Book Price : "+books_comp[index].getPrice());		
		}		
	}


}
