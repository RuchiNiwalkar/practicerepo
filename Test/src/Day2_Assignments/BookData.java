package Day2_Assignments;

public class BookData  {
	Book[] books_comp = new Book[2];
	private int index=0;
	public void setBook(int bookNo, String title, String publication, String author, float price, String type) 
	{
		books_comp[index] = new Computer();
		Computer comp =	(Computer)books_comp[index];
		//System.out.println("Enter book No : ");
		comp.setBookNo(bookNo);
		comp.setTitle(title);
		comp.setPublication(publication);
		comp.setAuthor(author);
		comp.setPrice(price);
		comp.setType(type);
		index++;

	}
	public void printBooks()
	{
		for(index=0 ; index< books_comp.length ;index++)
		{
			Computer comp =	(Computer)books_comp[index];
			if(comp.getType().equalsIgnoreCase("Networking"))
			{
				System.out.println();
				System.out.println("Book details : ");
				System.out.println("Book no : "+comp.getBookNo());
				System.out.println("Book Title : "+comp.getTitle());
				System.out.println("Book Publication : "+comp.getPublication());
				System.out.println("Book Author : "+comp.getAuthor());
				System.out.println("Book Price : "+comp.getPrice());
				System.out.println("Book Type : "+comp.getType());			
			}
		}		
	}


}
