package Day2_Assignments;

import java.util.Scanner;

public class TestRectangle {

	public static void main(String[] args)
	{
		
		Scanner s = new Scanner(System.in);
		Rectangle r = new Rectangle();
		Double TotalArea = r.calArea();
		System.out.println("The Area of rectangle  with Height "+r.getHeight()+" and width "+r.getWidth()+" is "+TotalArea);
		
		
		System.out.println("Enter the height of the Rectangle : ");
		r.setHeight(s.nextDouble());
		System.out.println("Enter the width of the Rectangle : ");
		r.setWidth(s.nextDouble());
		Double TotalArea_user = r.calArea();
		System.out.println("The Area of rectangle  with Height "+r.getHeight()+" and width "+r.getWidth()+" is "+TotalArea_user);
	}

}
