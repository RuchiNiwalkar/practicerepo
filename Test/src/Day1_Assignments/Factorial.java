package Day1_Assignments;

import java.util.Scanner;

public class Factorial 
{

	Scanner s = new Scanner(System.in);
	private int number;
	private int fact;

	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getFact() {
		return fact;
	}

	public void calFact()
	{
		fact=1;
		int i=1;
		for(i=1 ; i<=number ;i++)
		{
			fact = fact*i;
		}

	//	System.out.println(fact);
	}
	
}
