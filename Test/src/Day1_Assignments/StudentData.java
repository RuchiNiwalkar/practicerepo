package Day1_Assignments;

import java.util.Scanner;

public class StudentData {

	private Student[] students = new Student[2];
	Scanner s = new Scanner(System.in);
	private int i = 0;

	public void addStudent(Student student)
	{
		students[i] = student; //  write this
		i++;
	}

	public void setStudents()
	{
		for(int i=0 ; i< students.length ; i++)
		{
			System.out.println("Enter Student Roll Number : ");
			students[i].setRollNo(s.nextInt());
			System.out.println("Enter Student Name : ");
			students[i].setStudName(s.next());
			System.out.println("Enter Student Percentage : ");
			students[i].setPercentage(s.nextDouble());

		}

	}


	//Printing the Student Data
	public void printDetails()
	{
		for(int i=0 ; i< students.length ; i++)
		{
			System.out.println("The Student details are : " );
			System.out.println();
			System.out.println("The Student Roll no is : "+students[i].getRollNo());
			System.out.println("The Student Name is : "+students[i].getStudName());
			System.out.println("The Student Percentage is : "+students[i].getPercentage());

		}

	}
	public String searchByRollNo(int rollNo)
	{
		String Value= null;
		String Name=null;
		for(i = 0 ; i<students.length ; i++)
		{
			if(students[i].getRollNo()==rollNo)
			{
				Name = students[i].getStudName();
				Double n =students[i].getPercentage();
				Value = n.toString();
			}
		}

		return  Name+ " and "+Value;
	}

	public void searchByPer(double percentage)
	{
		for(i =0 ; i <students.length ; i++)
		{
			if(students[i].getPercentage()==percentage)
			{
				System.out.println("The Student details are : " );
				System.out.println();
				System.out.println("The Student Roll no is : "+students[i].getRollNo());
				System.out.println("The Student Name is : "+students[i].getStudName());
				System.out.println("The Student Percentage is : "+students[i].getPercentage());

			}

		}

	}

}
