package Day1_Assignments;

import java.util.Scanner;

public class BookDetails {

	private Book[] book = new Book[2];
	Scanner s = new Scanner(System.in);

	public void setBooks()
	{
		/*Book DanBrown = new Book();
		Book RLStine = new Book();

		book[0] = new Book();  //since i = 0, 1 ,2 .. increments in loop
		book[1] = new Book();

		//also can be written as 
		book[0] = DanBrown;  //since i = 0, 1 ,2 .. increments in loop
		book[1] = RLStine;
		 */
		for(int i=0; i<book.length ; i++)
		{

			book[i] = new Book(); //using loop in order to set or print 50 objects
			System.out.println("Enter book No : ");
			book[i].setBookNo(s.nextInt());
			System.out.println("Enter book Title : ");
			book[i].setTitle(s.next());
			System.out.println("Enter book Author : ");
			book[i].setAuthor(s.next());
			System.out.println("Enter book Price Float : ");
			book[i].setPrice(s.nextFloat());
			System.out.println("Enter book Publication : ");
			book[i].setPublication(s.next());
			System.out.println();
		}	

	}
	public void printBooks ()
	{
		System.out.println("Printing 2 books :");
		for(int i=0; i<book.length ; i++)
		{
			System.out.println("The book No. is : "+book[i].getBookNo());
			System.out.println("The book Title is : "+book[i].getTitle());
			System.out.println("The book Author is : "+book[i].getAuthor());
			System.out.println("The book Price is : "+book[i].getPrice());
			System.out.println("The book Publication is : "+book[i].getPublication());
			System.out.println();
			System.out.println();
		}

	}


	public void searchBookByTitle(String title)
	{
		boolean flag = false;
		int noofbooks= 0;
		int num=0;
		for(int i=0 ; i<book.length ; i++)
		{
			if(book[i].getTitle().equalsIgnoreCase(title))
			{
				noofbooks++;
				flag=true;
				num=i;
			}
		}

		if(flag==true)

		{
			System.out.println("The total number of books with same Title are : "+noofbooks);
			System.out.println("The book No. is : "+book[num].getBookNo());
			System.out.println("The book Title is : "+book[num].getTitle());
			System.out.println("The book Author is : "+book[num].getAuthor());
			System.out.println("The book Price is : "+book[num].getPrice());
			System.out.println("The book Publication is : "+book[num].getPublication());
		}
		else
		{
			System.out.println("The Book does not exist");
		}
	}

}



