package Collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Demo_Comparator 
{
	public static void main(String[] args)
	{
		List<Integer> values = new ArrayList<>();
		values.add(287);
		values.add(366);
		values.add(244);
		values.add(748);

		
		Comparator<Integer> comp = new Comparator<Integer>() 
		{
			@Override
			public int compare(Integer o1, Integer o2) 
			{
				if(o1%10>o2%10)
				{
					return 1;
				}

				return -1;
			}

		};

		Collections.sort(values , comp);

		for(int i: values)
		{
			System.out.println(i);
		}
	}

}
