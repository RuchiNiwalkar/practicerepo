package Collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
	
	public static void main(String[] args) 
	{
		
		Set<Integer> values = new TreeSet<>();	
		
		System.out.println(values.add(11));
		System.out.println(values.add(9));
		System.out.println(values.add(12));
		System.out.println(values.add(9));
		
		for(int i : values)
		{
			System.out.println(i);
		}
		
	}

}
