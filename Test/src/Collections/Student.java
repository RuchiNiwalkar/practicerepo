package Collections;

public class Student implements Comparable<Student>
{
	private int marks;
	private String name;
	
	
	public Student(int marks, String name) 
	{
		this.marks = marks;
		this.name = name;
	}

	

	public int getMarks() {
		return marks;
	}



	public void setMarks(int marks) {
		this.marks = marks;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}


	@Override
	public int compareTo(Student s) 
	{
		//return (this.name.compareToIgnoreCase(s.getName()));
		return(this.getName().compareToIgnoreCase(s.getName()));
	
	}




}
