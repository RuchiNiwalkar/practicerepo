package Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class StudentHashSet {

	Scanner sc = new Scanner(System.in);
	TreeSet<String> values = new TreeSet<>();

	public void setNames()
	{
		System.out.println("Enter 3 names of student");
		values.add(sc.next());
		values.add(sc.next());
		values.add(sc.next());
	}

	public void printNames()
	{
		System.out.println();
		System.out.println("The Names are : ");

		for(String i : values )
		{
			System.out.println(i);
		}

	}


	public void searchName(String stuName ) 
	{
		String name  = null;
		boolean flag=false;
		Iterator<String> i = values.iterator();
		while(i.hasNext())
		{	
			name=i.next();
			if(name.equalsIgnoreCase(stuName))
			{
				flag=true;
				break;		
			}
		}

		if(flag==true)
		{
			System.out.println("The name is : "+name);
		}
		else
		{
			System.out.println("The name does not exist in hashset");
		}

	}

}