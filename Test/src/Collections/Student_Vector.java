package Collections;

import java.util.Enumeration;
import java.util.Scanner;
import java.util.Vector;

public class Student_Vector
{
	Vector<String> v = new Vector();
	Scanner s = new Scanner(System.in);

	public void setNames()
	{
		System.out.println("Enter the names : ");
		while(!(s.hasNext("Done")||s.hasNext("done")))
		{
			v.add(s.next());

		}		
	}

	public void printNames()
	{
		Enumeration e = v.elements();
		System.out.println("The names are : ");
		while(e.hasMoreElements())
		{
			System.out.println(e.nextElement());

		}

	}

	public void searchName(String stuName ) 
	{
		boolean flag=false;
		int counter=0;
		for(int i=0 ;i <v.size() ;i++)
		{
			if(stuName.equalsIgnoreCase(v.get(i)))
			{
				flag=true;
				counter=i;
			}
		}
		if(flag==true)
		{
			System.out.println("Name is : "+v.get(counter));
		}
		else
		{
			System.out.println("Name is not present in the Vector");
		}

	}

	public void searchName(int index) 
	{

		boolean flag=false;
		int counter=0;
		for(int i=0 ;i <v.size() ;i++)
		{
			if(index==i)
			{
				flag=true;
				counter=i;
			}
		}
		if(flag==true)
		{
			System.out.println("Name present on "+ counter +" index is  : "+v.get(counter));
		}
		else
		{
			System.out.println("Index is not present in the Vector");
		}

	}

	public void removeName( String stuName ) 
	{

		boolean flag=false;
		int counter=0;
		for(int i=0 ;i <v.size() ;i++)
		{
			if(stuName.equalsIgnoreCase(v.get(i)))
			{
				flag=true;
				counter=i;
			}
		}
		if(flag==true)
		{
			
			System.out.println("The deleted Name is : "+v.remove(counter));
			printNames();
		}
		else
		{
			System.out.println("Name is deleted present in the Vector");
		}





	}


}
