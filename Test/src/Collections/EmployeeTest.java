package Collections;

import java.util.Enumeration;
import java.util.Vector;

public class EmployeeTest
{

	Vector<Employee> v = new Vector<>();

	Employee e1 = new Employee(10 , "Ruchi ", "A street");
	Employee e2 = new Employee(20 , "Riya ", "B street");

	public void set()
	{
		v.addElement(e1);
		v.addElement(e2);

	}

	public void display()
	{
		Enumeration<Employee> i = v.elements();
		while(i.hasMoreElements())
		{
			Employee emp = i.nextElement();

			System.out.println(emp.getEmployeeNo());
			System.out.println(emp.getEmployeeName());
			System.out.println(emp.getAddress());
			System.out.println();
		}
	}
}