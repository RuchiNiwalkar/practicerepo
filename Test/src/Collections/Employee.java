package Collections;

import java.util.Enumeration;
import java.util.Vector;

public class Employee {
	
	private int EmployeeNo;
	private String EmployeeName;
	private String Address;
	
	
	public int getEmployeeNo() {
		return EmployeeNo;
	}



	public void setEmployeeNo(int employeeNo) {
		this.EmployeeNo = employeeNo;
	}



	public String getEmployeeName() {
		return EmployeeName;
	}



	public void setEmployeeName(String employeeName) {
		this.EmployeeName = employeeName;
	}



	public String getAddress() {
		return Address;
	}



	public void setAddress(String address) {
		this.Address = address;
	}



	public Employee(int EmployeeNo, String EmployeeName , String Address )
	{
		this.EmployeeNo = EmployeeNo;
		this.EmployeeName = EmployeeName;
		this.Address = Address;	
	}
		
	
}
