package Collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class ArrayList_Type {

	Scanner s = new Scanner(System.in);

	List<String> names = new ArrayList<String>();

	//simple method
	/*public void setNames()
	{
		System.out.println("Enter names : ");

			names.add(s.next());
			names.add(s.next());
			names.add(s.next());

	}*/

	//user input in a loop
	public void setNames()
	{
		System.out.println("Enter names and type 'done' after entering values ");
		while(!(s.hasNext("Done")||s.hasNext("done")))
		{
			names.add(s.next());

		}

	}
	public void printNames() 
	{
		System.out.println();
		Iterator i = names.iterator();
		System.out.println("The Names are : ");
		while(i.hasNext())
		{
			System.out.println(i.next());
		}

	}

	public void searchName(String stuName)
	{
		boolean flag=false;
		int counter=0;
		for(int i=0 ; i< names.size() ; i++)
		{
			if(stuName.equalsIgnoreCase(names.get(i)))
			{
				{
					flag=true;
					counter=i;

				}
			}

		}

		if(flag==true)
		{

			System.out.println("The name is : "+names.get(counter));

		}
		else
		{
			System.out.println("The name to be searched is not present in arraylist ");
		}

	}


	public void searchName(int index) 
	{
		int counter=0;
		boolean flag=false;
		for(int i=0; i< names.size() ;i++)
		{

			if(index==i)
			{
				flag=true;
				counter=i;
			}	

		}
		if(flag==true)
		{
			System.out.println("The name present at "+index+" is "+names.get(counter));
		}
		else
		{
			System.out.println("Index not present in arraylist");
		}
	}

	public void removeName( String stuName ) 
	{
		boolean flag=false;
		int counter=0;
		for(int i=0 ; i< names.size() ; i++)
		{
			if(stuName.equalsIgnoreCase(names.get(i)))
			{
				{
					flag=true;
					counter=i;

				}
			}

		}

		if(flag==true)
		{

			System.out.println("The name "+names.remove(counter)+" is deleted");
			System.out.println();
			printNames();

		}
		else
		{
			System.out.println("The name to be deleted is not present in arraylist ");
		}

	}


}
