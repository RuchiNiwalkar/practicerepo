package Collections;

import java.util.Vector;

public class DemoVector {

	public static void main(String[] args) 
	{

		Vector<Integer> v = new Vector<>();
		v.add(8);
		v.add(4);
		v.add(3);
		v.add(1);
		
		System.out.println("Capacity is : "+v.capacity());

		v.remove(2);

		for(int i : v)
		{
			System.out.println(i);
		}
	}

}
