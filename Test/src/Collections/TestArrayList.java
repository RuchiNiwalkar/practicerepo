package Collections;

import java.util.Scanner;

public class TestArrayList {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		ArrayList_Type s = new ArrayList_Type();
		s.setNames();
		s.printNames();
		System.out.println("Enter the name to be searched : ");
		String search = sc.next();
		s.searchName(search);
		System.out.println("Enter the index position : ");
		int indexposition = sc.nextInt();
		s.searchName(indexposition);
		System.out.println("Enter the name to be deleted : ");
		String delete = sc.next();
		s.removeName(delete);
		
	}

}
