package Collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class StudentTest {

	public static void main(String[] args) 
	{
		Student s1 = new Student(22 , "Ankita");
		Student s2 = new Student(44 , "Riya");	
		Student s3 = new Student(20 , "Omkar");
		
		
		List<Student> students = new ArrayList<>();
		students.add(s1);
		students.add(s2);
		students.add(s3);
		
		//Collections.reverse(students);
		Collections.sort(students);
			
		for(Student i : students)
		{
			System.out.println(i.getMarks()+ " and "+i.getName());
		}

		
	}

}
