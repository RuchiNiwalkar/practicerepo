package Collections;

import java.util.Scanner;

public class Student_VectorTest {

	public static void main(String[] args)
	{
		Scanner s = new Scanner(System.in);
		Student_Vector sv = new Student_Vector();
		sv.setNames();
		sv.printNames();
		System.out.println();
		System.out.println("Enter the name to be searched : ");
		sv.searchName(s.next());
		System.out.println();
		System.out.println("Enter the index : ");
		sv.searchName(s.nextInt());
		System.out.println();
		System.out.println("Enter the name to be deleted : ");
		sv.removeName(s.next());

	}

}
