package Collections;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TestTreeSet {

	public static void main(String[] args)
	{

		TreeSet<String> product = new TreeSet<>();
		product.add("Laptops");
		product.add("Fridge");
		product.add("AC");
		product.add("Laptops");
		product.add("Bulbs");

		//does not print duplicate products
		Iterator<String> i = product.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next());
			
		}
		System.out.println();
		System.out.println("First Product Name is : "+product.first());
		System.out.println();
		System.out.println("Last Product Name is : "+product.last());
		System.out.println();
		System.out.println("SIZE OF TREESET : "+product.size());
		System.out.println();
		product.remove("Laptops");
		Iterator<String> j = product.iterator();
		while(j.hasNext())
		{
			System.out.println(j.next());
			
		}
		System.out.println();
		System.out.println("SIZE OF TREESET : "+product.size());
	}

}
