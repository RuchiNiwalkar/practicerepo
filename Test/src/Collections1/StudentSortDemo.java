package Collections1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class StudentSortDemo {

	public static void main(String[] args)
	{

		List<Student> students = new ArrayList<>();

		Student s1 = new Student(22 , "Ruchi");
		Student s2 = new Student(33, "Ankita");

		students.add(s1);
		students.add(s2);


		Iterator<Student> j = students.iterator();
		while(j.hasNext())
		{
			Student s = j.next();
			System.out.println(s.getName()+ "  "+s.getRollNo());
		}

		System.out.println();


		Comparator<Student> comp = new Comparator<Student>() 
		{

			@Override
			public int compare(Student o1, Student o2) 
			{
				return (o1.getName().compareTo(o2.getName()))
						;

			}


		};

		Collections.sort(students , comp);

		Iterator<Student> i = students.iterator();
		while(i.hasNext())
		{
			Student s = i.next();
			System.out.println(s.getName()+ "  "+s.getRollNo());
		}


	}

}
