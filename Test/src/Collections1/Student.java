package Collections1;

public class Student implements  Comparable<Student>
{
	private int rollNo;
	private String name;
	
	
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	//constructor
	public Student(int rollNo, String name) 
	{

		this.rollNo = rollNo;
		this.name = name;
	}


	//to string method
	@Override
	public String toString() 
	{
		return "Student [rollNo=" + rollNo + ", name=" + name + "]";
	}

	//Comparator
	@Override
	public int compareTo(Student o) 
	{
		return this.getName().compareTo(o.getName());
	
	}













}
