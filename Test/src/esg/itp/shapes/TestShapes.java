package esg.itp.shapes;

public class TestShapes {

	public static void main(String[] args) 
	{	
		Square s = new Square(2.2);
		
		s.calcArea();
		s.calcPeri();
		s.display();
		
		
		Rectangle r = new Rectangle(2.0, 3.0);
		r.calcArea();
		r.calcPeri();
		r.display();
	}

}
