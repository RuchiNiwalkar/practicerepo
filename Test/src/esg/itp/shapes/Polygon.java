package esg.itp.shapes;

import java.util.Scanner;

public interface Polygon {
	
	Scanner s = new Scanner(System.in);
	double area=0;
	double perimeter=0 ;

	public double getArea();

	public double getPerimeter();
	

	void calcArea();
	void calcPeri();
	void display();
	
	
	
	
	

}
