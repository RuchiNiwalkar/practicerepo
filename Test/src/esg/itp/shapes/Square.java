package esg.itp.shapes;

public class Square implements Polygon{

	double side ;
	

	public Square(double side )
	{
		this.side = side;
		
	}
	
	@Override
	public void calcArea()
	{
		Double AREA = getArea();
		
	}

	@Override
	public void calcPeri()
	{
		Double PERIMETER = getPerimeter();
		
	}

	@Override
	public void display() 
	{
		Double AREA = getArea();
		Double PERIMETER = getPerimeter();
		System.out.println("The area of Square is : "+AREA);
		System.out.println("The Perimeter of Square is : "+PERIMETER);
		
		
	}

	@Override
	public double getArea() 
	{
		return side*side ;
	}

	@Override
	public double getPerimeter()
	{
		return 4*side;
	}
	
	
	
	
	
	
	
	
	

}
