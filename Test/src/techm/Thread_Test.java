package techm;

public class Thread_Test
{	
	public static void main(String[] args) throws InterruptedException
	{
		MyThread  mythread = new MyThread();
		YourThread yourthread = new YourThread();
		
		Thread t1 = new Thread(mythread);
		Thread t2 = new Thread(yourthread);
		

		t1.setPriority(Thread.MAX_PRIORITY);
	
		t2.setPriority(Thread.MIN_PRIORITY);
		

		Thread.sleep(2000);
		t1.start();
		Thread.sleep(1000);
		t2.start();
		

	
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		
		
		System.out.println("State of Thread t1: "+t1.isAlive());
		System.out.println("Bye");


	}

}
