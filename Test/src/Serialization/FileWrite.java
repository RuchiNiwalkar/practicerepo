package Serialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileWrite {

	public static void main(String[] args) throws Exception
	{
		File f = new File("letter.txt");
		//Scanner s = new Scanner(System.in);
		FileOutputStream fos= new FileOutputStream(f);
		DataOutputStream dos = new DataOutputStream(fos);
		dos.writeUTF("Hello");
		
		FileInputStream fis = new FileInputStream(f);
		DataInputStream dis = new DataInputStream(fis);
		String text = dis.readUTF();
		System.out.println(text);

		fos.close();
		dos.close();
		fis.close();
		dis.close();

		if(f.delete())
		{
			System.out.println("Deleted succesfully");
		}

		else
		{
			System.out.println("Not deleted successfully");
		}
	}

}
