package Serialization;

import java.io.Serializable;

public class CDR implements Serializable{

	private int sourceNumber ;
	private int destinationNumber ;
	private double duration ;
	private double charge;


	public int getSourceNumber() {
		return sourceNumber;
	}
	public void setSourceNumber(int sourceNumber) {
		this.sourceNumber = sourceNumber;
	}
	public int getDestinationNumber() {
		return destinationNumber;
	}
	public void setDestinationNumber(int destinationNumber) {
		this.destinationNumber = destinationNumber;
	}
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public double getCharge() {
		return charge;
	}
	public void setCharge(double charge) {
		this.charge = charge;
	}


	public CDR(int sourceNumber, int destinationNumber, double duration)
	{	
		this.sourceNumber = sourceNumber;
		this.destinationNumber = destinationNumber;
		this.duration = duration;
	}

	public void calcuateCharge()
	{
		this.charge = this.duration * 1 ;
	
	}






}
