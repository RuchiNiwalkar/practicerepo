package Serialization;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DirectoryList 
{

	public static void main(String[] args) 
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a directory name : ");
		String directoryname = s.next();
		File directory = new File(directoryname);
		File[] fList = directory.listFiles();
		/*directories.add(new File(directoryname));
		directories.add(new File("F:\\eclipse\\Folder2"));
		directories.add(new File("F:\\eclipse\\Folder3"));
		 */
		//  F:\eclipse\Folder1
		if(directory.exists())
		{
			for (File file : fList)
			{
				if(file.isFile())
				{
					System.out.println("The files in given directory are : "+file.getName());
				}

				if(file.isDirectory())
				{
					String name= file.getAbsolutePath();
					File f2 = new File(name);
					File[] names = f2.listFiles(); 
					System.out.println("Filenames in sub directories : " +f2.getName());
					for(File n : names)
					{
						System.out.println(n.getName());
					}

				}

			}

		}
		else
		{
			System.out.println("Directory does not exist");
		}

		/*for(File f : directories)
		{
			if(f.mkdir())
			{
				System.out.println(" SUCESS "+ f.getPath());			

			}
			else
			{
				System.out.println(f.getPath()+" folder already exist");

			}

		}*/

	}

}
