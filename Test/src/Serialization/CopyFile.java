package Serialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Formatter;
import java.util.Scanner;

public class CopyFile {

	public static void main(String[] args) throws Exception 
	{

		File sf = new File("F:\\eclipse\\demo.txt");
		File df = new File("F:\\eclipse\\DestinationFile.txt");
		Scanner s = new Scanner(System.in);

		if(!sf.exists())
		{
			sf.createNewFile();
		}
		else
		{
			System.out.println("Source file exists");
		}
		
		
		if(df.exists())
		{
			System.out.println("Want to overright ? ");
			System.out.println("Enter yes or no");
			String answer = s.next();

			if(answer.equalsIgnoreCase("yes"))
			{
				System.out.println("OverWritten");
				FileOutputStream fos= new FileOutputStream(df);
				DataOutputStream dos = new DataOutputStream(fos);
				dos.writeChars("This file is overwritten");
			}
			else
			{
				System.out.println("Not OverWritten");
	
			}
		}
		else
		{
			Files.copy(sf.toPath(), df.toPath());
		}

	}
}
